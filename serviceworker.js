const PRECACHE = 'precache-v1';

const PRECACHE_URLS = [
  'offline.html'
]

self.addEventListener('install', event => {
  event.waitUntil(
    caches.open(PRECACHE)
      .then(cache => {
        cache.addAll(PRECACHE_URLS)
      })
      .then(self.skipWaiting())
  )
})

self.addEventListener('fetch', event => {
  if(event.request.url.split('#')[0].endsWith('/offline.html')){
    event.respondWith(
      caches.open(PRECACHE).then(function(cache) {
        return fetch(event.request)
          .then(function(response) {
            cache.put(event.request, response.clone())
            return response
          })
          .catch(e => {
            return cache.match(event.request)
          })
      })
    )
  }
})
