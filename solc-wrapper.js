#!/usr/bin/env node

var fs = require('fs')
var solc = require('solc')
var program = require('commander')
var Promise = require('bluebird')
 
program
  .option('-v, --compiler <version>', 'Compiler version')
  .option('-f, --file <file>', 'Input file')
  .option('-a, --ABI <file>', 'ABI file')
  .option('-b, --bytecode <file>', 'Bytecode file')
  .option('-n, --contractName <name>', 'Bytecode file')
  .parse(process.argv)
 
console.log('bytecode', program.bytecode)
console.log('compiler version', program.compiler)
console.log('ABI', program.ABI)
console.log('program file', program.file);
console.log('contract name', program.contractName);

(program.compiler == null || program.compiler.trim() == ""
  ? Promise.resolve(solc)
  : Promise.promisify(solc.loadRemoteVersion)(program.compiler)
).then(compiler => {
  var source = fs.readFileSync(program.file, 'utf-8')
  var output = compiler.compile(source, 1)
  output.errors.forEach(error => console.log(error))
  var contract = output.contracts[program.contractName] || 
    output.contracts[':' + program.contractName]
  if(contract && contract.bytecode && contract.interface){
    function write(name, content){
      fs.writeFileSync(name, content, 'utf-8')
    }
    write(program.bytecode, contract.bytecode)
    write(program.ABI, 
      JSON.stringify(
        JSON.parse(contract.interface), null, 2
      )
    )
  } else {
    console.log('Failed to compile contract')
    process.exit(1)
  }
})

