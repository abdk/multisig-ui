#!/usr/bin/env node

var inline = require('inline-html')
var fs = require('fs')

var FILE_TO_INLINE = 'offline.html'
 
inline.file(FILE_TO_INLINE)
  .then(html => fs.writeFileSync(FILE_TO_INLINE, html, 'utf-8'))
