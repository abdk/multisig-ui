define(function(require){

var core = require('core')
var storage = require('browserStorage')
var walletABI = require('./wallet/Wallet.abi')
var tokenABI = require('./wallet/Token.abi')
var walletCode = {
  tokenFriendly: require('./wallet/TokenFriendlyWallet.bin'),
  standard: require('./wallet/Wallet.bin'),
}
var abi = {
  tokenFriendly: require('./wallet/TokenFriendlyWallet.abi'),
  standard: walletABI,
}
var {callWeb3, wrapContractMethod} = require('utils/web3')
var {promisedProperties} = require('utils')

function logDecoder(abi){
  var eventEntries = abi.filter(function(e){
    return e.type === 'event'
  })
  var events = new AllSolidityEvents(null, eventEntries, null)
  return events.decode.bind(events)
}

var decodeWalletLogs = logDecoder(walletABI)

function getLogs(contractAddress, {fromBlock, toBlock}){
  return callWeb3(web3.eth.getLogs)({
    fromBlock: web3.toHex(fromBlock),
    toBlock: web3.toHex(toBlock),
    topics: [],
    address: contractAddress,
  }).then(evs => evs.map(decodeWalletLogs))
}

abiDecoder.addABI(walletABI)
abiDecoder.addABI(tokenABI)

core.onDataChanged('newBlock', updatePendingTransactions)
core.onDataChanged('newBlock', updatePendingWallets)

function deploymentData(walletType, ...args){
  log('deployMent data', ...args)
  var contract = new Web3().eth.contract(abi[walletType])
  var data = walletCode[walletType]
  return contract.new.getData(...args, {data})
}

function executeData({token, to, amount, customData}){
  var data
  if(token == 'ether'){
    data = new Web3().eth.contract(walletABI).at()['execute']
      .getData(to, amount, customData);
  } else {
    var tokenData = new Web3().eth.contract(tokenABI).at()['transfer'].getData(to, amount);
    data = new Web3().eth.contract(walletABI).at()['execute'].getData(token, 0, tokenData);
  }
  log('executeData', {token, to, amount, customData}, 'result data is', data)
  return data
}

function burnTokensData({token, amount}){
  var tokenData = new Web3().eth.contract(tokenABI).at()['burnTokens'].getData(amount);
  var data = new Web3().eth.contract(walletABI).at()['execute'].getData(token, 0, tokenData);
  log('burnTokensData', {token, amount}, 'result data is', data)
  return data
}

function confirmData(operation){
  return new Web3().eth.contract(walletABI).at().confirm.getData(operation)
}

function fetchTransactionDetails(txHash){
  return callWeb3(web3.eth.getTransactionReceipt)(txHash).then(receipt => {
    if(receipt == null){
      return
    }
    return getLogs(receipt.to, {fromBlock: receipt.blockNumber, toBlock: receipt.blockNumber})
    .then(events => {
      var singleTransactEvent = events
        .find(ev => ev.transactionHash == txHash && ev.event == 'SingleTransact')
      var isSingleTransact = singleTransactEvent != null
      if(isSingleTransact){
        return {receipt, isSingleTransact: true}
      } else {
        var confirmationNeeded = events
          .find(ev => ev.transactionHash == txHash && ev.event == 'ConfirmationNeeded')
        var operation = confirmationNeeded && confirmationNeeded.args.operation
        return {receipt, isSingleTransact: false, operation}
      }
    })
  })
}

function updatePendingTransactions(){
  console.log('update pending transactions')
  var txs = storage.load('transactions')
  var pending = txs.filter(tx => tx.receipt == null)
  return Promise.all(pending.map(tx => 
    fetchTransactionDetails(tx.transaction.hash)
      .then(data => {
        if(data == null){
          return
        }
        var {receipt, operation, isSingleTransact} = data
        tx.receipt = receipt
        tx.metadata.operation = operation
        tx.metadata.isSingleTransact = isSingleTransact
      })
    )
  ).then(() => {
    storage.save('transactions', txs)
    core.dataChanged('transactions')
  })
}

function updatePendingWallets(){
  console.log('update pending wallets')
  var wallets = storage.load('wallets')
  var pending = wallets.filter(wallet => wallet.address == null)
  return Promise.all(pending.map(wallet => 
    callWeb3(web3.eth.getTransactionReceipt)(wallet.transactionHash)
      .then(receipt => {
        if(receipt != null){
          wallet.address = receipt.contractAddress
        }
      })
    )
  ).then(() => {
    console.log('data changed wallets')
    storage.save('wallets', wallets)
    core.dataChanged('wallets')
  })
}

function parseTransaction(rawTx){
  var tx = new EthJS.Tx(rawTx)
  var data = '0x' + tx.data.toString('hex')
  return parseTransactionData(data)
}

function parseTransactionData(data){
  var result = abiDecoder.decodeMethod(data)
  var type
  if(result.name == 'execute' || result.name == 'confirm'){
    type = result.name
  } else {
    type = 'unknown'
  }
  if(type == 'execute'){
    return Object.assign({type}, parseTransactionParams({
      to:    result.params[0].value,
      value: result.params[1].value,
      data:  result.params[2].value,
    }))
  } else {
    return {type}
  }
}

function parseTransactionParams({value, data, to}){
  if(data == null || data == '' || data == '0x'){
    return {
      isValid: true,
      to,
      value: Web3.prototype.toBigNumber(value),
    }
  } else {
    var result = abiDecoder.decodeMethod(data)
    if(result == null){
      return {
        isValid: true, 
        name: 'transfer',
        value: Web3.prototype.toBigNumber(value),
        to,
      }
    }
    if(result.name == 'createTokens' || result.name == 'burnTokens'){
      if(result.params.length != 1){
        return {
          isValid: false, 
          data,
          message: 'Invalid params count: expected 1, actual ' + result.params.length,
          value: Web3.prototype.toBigNumber(value),
        }
      } else {
        return {
          name: result.name,
          isValid: true,
          value: Web3.prototype.toBigNumber(result.params[0].value),
          token: to,
        }
      }
    } else if(result.name == 'transfer'){
      if(result.params.length != 2){
        return {
          isValid: false, 
          data,
          message: 'Invalid params count: expected 2, actual ' + result.params.length,
          value: Web3.prototype.toBigNumber(value),
        }
      }
      return {
        name: result.name,
        isValid: true,
        to: result.params[0].value,
        value: Web3.prototype.toBigNumber(result.params[1].value),
        token: to,
      }
    } else {
      return {isValid: false, data, message: 'Unknown method: ' + result.name, value: Web3.prototype.toBigNumber(value)}
    }
  }
}

var BLOCKS_IN_CHUNK = 50000

function fetchEventsInChunks(walletAddress, filterPredicate, findPredicate){

  function fetchChunk(blockNumber){
    console.log('fetch chunk', blockNumber, Math.max(0, blockNumber - BLOCKS_IN_CHUNK))
    return getLogs(walletAddress, {
      fromBlock: Math.max(0, blockNumber - BLOCKS_IN_CHUNK),
      toBlock: blockNumber,
    }).then(events => {
      return events.filter(filterPredicate)
    })
  }

  function doFetchEventsInChunks(blockNumber, eventsAcc = []){
    if(blockNumber < 0){
      return Promise.resolve(eventsAcc)
    } else {
      return fetchChunk(blockNumber).then(events => {
        var nextEventsAcc = events.concat(eventsAcc)
        if(events.find(findPredicate) != null){
          console.log('found')
          return nextEventsAcc
        } else {
          return doFetchEventsInChunks(
            blockNumber - BLOCKS_IN_CHUNK - 1, 
            nextEventsAcc
          )
        }
      })
    }
  }

  return callWeb3(web3.eth.getBlockNumber)().then(blockNumber => 
    doFetchEventsInChunks(blockNumber, [])
  )
}

function fetchContractProp(propName, propType, address){
  var contract = web3.eth.contract([
    {
      "constant": true,
      "inputs": [],
      "name": propName,
      "outputs": [
        {
          "name": 'unused',
          "type": propType,
        }
      ],
      "payable": false,
      "stateMutability": "view",
      "type": "function"
    },
  ]).at(address)
  return wrapContractMethod(contract, propName)()
    .catch(e => null)
}

function fetchDecimals(tokenAddress){
  return fetchContractProp('decimals', 'uint8', tokenAddress)
}

function fetchOperationStatus(wallet, operation){
  var contract = web3.eth.contract(walletABI).at(wallet)
  var EVENTS = ['ConfirmationNeeded',  'Confirmation', 'MultiTransact']

  return promisedProperties({
    events: fetchEventsInChunks(
      wallet, 
      ev => ev.args.operation == operation && EVENTS.indexOf(ev.event) != -1,
      ev => ev.event == 'ConfirmationNeeded'
    ),
    required: callWeb3(contract.m_required)(),
  }).then(({events, required}) => {
      console.log('evs', events)
      var confirmationNeeded = events.find(ev => ev.event == 'ConfirmationNeeded')
      if(confirmationNeeded == null){
        return {status: 'notfound'}
      }
      var status = events.find(ev => ev.event == 'MultiTransact') != null
        ? 'completed'
        : 'confirming'
      var result =  {
        status,
        params: parseTransactionParams(confirmationNeeded.args),
        required: required.toNumber(),
        confirmations: _.uniq(
          events
            .filter(ev => ev.event == 'Confirmation')
            .map(ev => ev.args.owner)
        )
      }
      if(result.params.token == null){
        return result
      } else {
        return fetchDecimals(result.params.token).then(decimals => {
          result.params.decimals = decimals
          return result
        })
      }
    })
}

function sendRawTransaction(rawTx){
  var tx = new EthJS.Tx(rawTx)
  var hash = '0x' + tx.hash(true).toString('hex')
  return callWeb3(web3.eth.getTransaction)(hash).then(tx => {
    if(tx != null){
      return tx
    } else {
      return callWeb3(web3.eth.sendRawTransaction)(rawTx).then(hash => {
        var ethTx = new EthJS.Tx(rawTx)
        var props = ["from", "nonce", "gasPrice", "gasLimit", "to", "value", "data"]
        var tx = {}
        for(var prop of props){
          tx[prop] = EthJS.Util.bufferToHex(ethTx[prop])
        }
        if(tx.to == '' || tx.to == '0x'){
          tx.to = null
        }
        tx.value = Web3.prototype.toBigNumber(
          tx.value == '0x' ? '0x0' : tx.value
        )
        tx.hash = hash
        return tx
      })
    }
  }).then(tx => {
    if(tx.to == null){
      var wallets = storage.load('wallets')
      wallets.unshift({
        transactionHash: tx.hash, 
        date: new Date(),
      })
      storage.save('wallets', wallets)
    } else {
      var txs = storage.load('transactions')
      if(txs.find(t => t.transaction.hash == tx.hash) == null){
        txs.unshift({
          transaction: tx, 
          metadata: Object.assign(
            {date: new Date()},
            parseTransaction(rawTx)
          ),
        })
        storage.save('transactions', txs)
      }
    }
    return tx
  })
}

return {
  fetchTransactionDetails,
  fetchOperationStatus,
  deploymentData,
  executeData,
  confirmData,
  burnTokensData,
  sendRawTransaction,
}
 
})
