define(function(require){

var Utils = {
  readFile(file){
    var reader = new FileReader();
    var p = new Promise(function(resolve){
      reader.onload = function(e) {
        resolve(e.target.result)
      }
    })
    reader.readAsText(file);
    return p
  },

  readFiles(fileList){
    var files = []
    for(var i = 0; i < fileList.length; i++){
      files.push(fileList[i]);
    }
    return Promise.all(files.map(Utils.readFile))
  },

  openFile: (() => {
    // reuse single input instance to workaround this bug:
    // https://stackoverflow.com/questions/39399947/event-onchange-wont-trigger-after-files-are-selected-from-code-generated-input
    // https://stackoverflow.com/questions/38845015/inputtype-file-change-event-are-lost-occasionally-on-chrome
    var input = document.createElement('input')
    input.setAttribute('type','file')
    var inputPromiseResolve
    input.addEventListener('click', function(){
      //https://stackoverflow.com/a/12102992
      this.value = null
    });
    input.addEventListener('change', function({target}){
      inputPromiseResolve(target.files);
    });
    return options => {
      options = options ? options : {};
      var accept = options.accept;
      if(options.multiple){
        input.setAttribute('multiple','true');
      } else {
        input.removeAttribute('multiple');
      }
      if(accept){
        input.setAttribute('accept', accept);
      } else {
        input.removeAttribute('accept');
      }
      var p = new Promise(resolve => inputPromiseResolve = resolve)
      input.click()
      return p
    }
  })(),
}

return Utils

})
