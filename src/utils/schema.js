define(function(require){

var TypeOf = type => ({
  name: type,
  test: value => typeof(value) == type 
    ? null 
    : ['not a ' + type + ': ' + value],
})

var Bool = TypeOf('boolean')

var Str = TypeOf('string')

var Num = TypeOf('number')

var NumWithLimits = (min, max) => ({
  name: '' 
    + (min == null ? '' : '>= ' + min)
    + (max == null ? '' : ' and <= ' + max),
  test: num => {
    var result = Num.test(num)
    if(result != null){
      return result
    }
    if(min != null && num < min){
      return [num + ': less than min ' + min]
    }
    if(max != null && num > max){
      return ['greater than max ' + max]
    }
    return null
  }
})

var Re = regexp => ({
  name: '' + regexp,
  test: value => regexp.test(value)
    ? null
    : ['not a ' + regexp + ' : ' + value]
})

var BigNum = {
  name: 'bignumber',
  test: (val) => (val && val.constructor && val.constructor.name) == 'BigNumber'
    ? null
    : ['not a bignumber: ' + val],
  toJSON: num => num.toString(),
  fromJSON: str => Web3.prototype.toBigNumber(str),
}

var Dt = {
  name: 'date',
  test: val => val instanceof Date 
    ? null 
    : ['not a date: ' + val],
  toJSON(date){return date.toJSON()},
  fromJSON(str){return new Date(str)},
}

var Enum = (...values) => ({
  name: 'enum(' + values.join(',') + ')',
  test: val => values.indexOf(val) == -1 
    ? ['not a valid enum value: ' + val]
    : null
})

var Optional = type => ({
  name: 'optional ' + type.name,
  test: val => val == null ? null : type.test(val),
  toJSON: val => {
    if(val == null){
      return null
    }
    return type.toJSON == null ? val : type.toJSON(val)
  },
  fromJSON: val => {
    if(val == null){
      return null
    }
    return type.fromJSON == null 
      ? val
      : type.fromJSON(val)
  }
})

var Either = (type1, type2) => {
  var name = 'either(' + type1.name + ', ' + type2.name + ')'
  return {
    name,
    test: value => {
      var error = type1.test(value)
      if(error == null){
        return null
      }else{
        error = type2.test(value)
        if(error == null){
          return null
        } else {
          return ['not a ' + name + ': ' +value]
        }
      }
    }
  }
}

var Arr = element => ({
  name: 'array of ' + element.name,
  test: array => {
    if(!Array.isArray(array)){
      return ['not an array: ' + array]
    }
    var firstError = null
    var idx = array.findIndex(x =>
      firstError = element.test(x)
    )
    if(firstError == null){
      return null
    }
    return firstError.concat('in ' + idx + ' element of array')
  },
  toJSON: arr => arr.map(element.toJSON || (x => x)),
  fromJSON: arr => arr.map(element.fromJSON || (x => x)),
})

// map implemented as object
// only string and regexp keys are implemented
var ObjMap = (keyType, valueType) => {
  function convert(value, converter){
    var result = {}
    for(var k in value){
      if(value.hasOwnProperty(k)){
        result[k] = converter == null 
          ? value[k] 
          : converter(value[k])
      }
    }
    return result
  }
  return {
    name: 'map<'+keyType.name+','+valueType.name+'>',
    test: map => {
      if(typeof(map) != 'object' || Array.isArray(map)){
        return ['not an object ' + map]
      }
      var keys = Object.keys(map)
      var firstError
      keys.find(key => 
        firstError = keyType.test(key)
      )
      if(firstError != null){
        return firstError.concat('as key of object')
      }
      var key = keys.find(key => 
        firstError = valueType.test(map[key])
      )
      if(firstError != null){
        return firstError.concat(
          'as value of object with key ' + key
        )
      }
      return null
    },
    toJSON: value => {
      return convert(value, valueType.toJSON)
    },
    fromJSON: value => {
      return convert(value, valueType.fromJSON)
    }
  }
}

var FreeFormObj = objectType => {
  var keys = Object.keys(objectType)
  var name = {}
  keys.forEach(key =>
    name[key] = objectType[key].name
  )
  function convert(converter){
    return function(o){
      var keys = Object.keys(o)
      var result = {}
      keys.forEach(k => {
        var propType = objectType[k]
        if(propType == null || converter(propType) == null){
          result[k] = o[k]
        } else {
          result[k] = converter(propType)(o[k])
        }
      })
      return result
    }
  }
  return {
    name: JSON.stringify(name, null, 2),
    test: value => {
      if(typeof(value) != 'object' || Array.isArray(value)){
        return ['not an object ' + value]
      }
      var firstError
      var key = keys.find(key => 
        firstError = objectType[key].test(value[key])
      )
      if(firstError == null){
        return null
      }
      return firstError.concat('in property ' + key + ' of object')
    },
    fromJSON: convert(o => o.fromJSON),
    toJSON: convert(o => o.toJSON),
  }
}

var Obj = objectType => {
  var o = FreeFormObj(objectType)
  function convert(converter){
    return function(o){
      var result = {}
      var keys = Object.keys(objectType)
      keys.forEach(k => {
        var propType = objectType[k]
        result[k] = converter(propType) == null 
          ? o[k]
          : converter(propType)(o[k])
      })
      return result
    }
  }
  return {
    name: 'strict ' + o.name,
    test: (value) => {
      var res = o.test(value)
      if(res != null){
        return res
      }
      var valueKeys = Object.keys(value)
      var unknownKey = valueKeys.find(k => 
        objectType[k] == null
      )
      if(unknownKey){
        return ['unknown key: ' + unknownKey]
      }
    },
    toJSON: convert(prop => prop.toJSON),
    fromJSON: convert(prop => prop.fromJSON),
  }
}

return {
  Bool, Num, NumWithLimits, Str, Re, BigNum, Dt, Enum,
  Either, Optional, Arr, ObjMap, Obj, FreeFormObj
}

})
