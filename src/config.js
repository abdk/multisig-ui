define(function(){
  return {
    APP_VERSION: '1.0',
    APP_NAME: 'multisig-ui',
    DEFAULT_LOCALE: 'en',
    CONFIRMATION_COUNT: 12,
    INFURA_ACCESS_TOKEN: 'WhddDV5MSfbY3bzY5jfQ',
    // prevent content_size_too_large error from node
    MAX_WEB3_BATCH_SIZE: 300,
    DEFAULT_GAS_PRICE: '20000000000',
    DEFAULT_GAS_LIMIT: 300000,
    getGasLimit(estimatedGasLimit){
      return Math.ceil(estimatedGasLimit * 2)
    }
  }
});
