define(function(require){

var config = require('config')
var {Stateful} = require('core')
var {burnTokensData} = require('wallet')
var {signTransaction} = require('transfer')
var storage = require('browserStorage')
var FormMessage = require('widgets/formMessage')
var getErrorMessage = require('./getErrorMessage')
var TokenSelect = require('widgets/tokenSelect')
var FormGroup = require('widgets/formGroup')
var Button = require('widgets/button')
var AccountSelect = require('widgets/accountSelect')
var DecimalsAwareAmountFormGroup = require('widgets/decimalsAwareAmountFormGroup')
var {OfflineTransactionBlock, isValid} = require('ui/offlineTransactionBlock')
var Form = require('widgets/form')

var {div, a} = React.DOM

function CreateBurnTokensTransactionState({stateChanged}){
  return {
    tokens: storage.loadFromMainDB('tokens').map(t => Object.assign(t, {
      decimals: t.decimals && parseInt(t.decimals)
    })),
    wallets: storage.loadFromMainDB('walletBook'),
    params: {
      gasPrice: Web3.prototype.toBigNumber(config.DEFAULT_GAS_PRICE),
      gasLimit: config.DEFAULT_GAS_LIMIT,
    },

    isValid(){
      return true
        && this.wallet != null
        && this.amount != null
        && this.token != null
        && isValid(this.params)
    },

    setToken(token){
      this.token = token
      this.amount = null
      stateChanged()
    },
    
    getToken(){
      return this.tokens.find(token => token.address == this.token)
    },

    create(){
      this.error = null
      this.isLoading = true
      stateChanged()
      var tx = {
        to: this.wallet,
        gasLimit: this.params.gasLimit,
        gasPrice: this.params.gasPrice,
        nonce: this.params.nonce,
        chainId: this.params.chainId,
        data: burnTokensData({
          token: this.token, 
          amount: this.amount,
        }),
      }
      return signTransaction(tx, this.params.signData)
        .then(signedTx => {
          this.isLoading = false
          console.log('signed', signedTx)
          saveAs(
            new Blob([signedTx]), 
            'transaction-' 
            + Web3.prototype.toChecksumAddress(this.wallet) 
            + '-'
            + new Date().toLocaleString()
          )
          stateChanged()
        })
        .catch(e => {
          this.isLoading = false
          this.error = getErrorMessage(e)
          stateChanged()
        })

    },
  }
}

return Stateful(CreateBurnTokensTransactionState, function({state, link}){
  return Form({onSubmit: state.create},
    FormGroup({
      labelMsg: 'Wallet',
      input: div({className: 'form-input-addon'},
        AccountSelect({
          accounts: state.wallets,
          valueLink: link('wallet'),
          placeholder: 'Wallet',
        }),
        a({href: '#wallets'}, 'Create wallet')
      )
    }),
    FormGroup({
      labelMsg: 'Token',
      input: TokenSelect({
        valueLink: {
          value: state.token,
          requestChange: state.setToken,
        },
        tokens: state.tokens,
      })
    }),
    state.token != null &&
      DecimalsAwareAmountFormGroup({
        key: state.token,
        valueLink: link('amount'),
        token: state.getToken(),
      }),
    OfflineTransactionBlock({valueLink: link('params')}),
    state.error && 
      FormMessage({
        message: state.error,
        status: 'error',
      }),
    div({className: 'action-button-row'},
      Button({
        btnType: 'primary',
        disabled: !state.isValid(),
        title: 'Sign and export transaction for Multisig Online',
        isLoading: state.isLoading,
      })
    )
  )
})

})
