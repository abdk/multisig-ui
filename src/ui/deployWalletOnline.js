define(function(require){

var storage = require('browserStorage')
var {deploymentData} = require('wallet')
var {sendTransaction} = require('transfer')
var {DeployWalletParamsState, DeployWalletParamsComponent} = 
  require('ui/deployWalletParams')
var getErrorMessage = require('./getErrorMessage')
var {Stateful} = require('core')
var Button = require('widgets/button')
var FormMessage = require('widgets/formMessage')
var Form = require('widgets/form')
var {div, a} = React.DOM

function getInitialState({stateChanged}){
  return {
    
    walletParams: new DeployWalletParamsState(),

    isValid(){
      return this.walletParams.isValid()
    },

    deploy(){
      this.isDeploying = true
      this.error = null
      stateChanged()
      sendTransaction({
        data: deploymentData(this.walletParams.walletType, ...this.walletParams.args())
      }, this.walletParams.signData)
      .then(tx => {
        var wallets = storage.load('wallets') || []
        wallets.unshift({
          transactionHash: tx.hash,
          date: new Date(),
          daylimit: this.walletParams.daylimit,
          owners: this.walletParams.effectiveOwners(),
          required: this.walletParams.required,
          walletType: this.walletParams.walletType,
        })
        storage.save('wallets', wallets)
        window.location.hash = 'wallets'
      }).catch(e => {
        this.isDeploying = false
        this.error = getErrorMessage(e)
        stateChanged()
        throw e
      })
    },

  }
}

return Stateful(getInitialState, function({state, link, stateChanged}){
  return Form({onSubmit: state.deploy},
    DeployWalletParamsComponent({
      state: state.walletParams,
      link(prop){
        return {
          value: state.walletParams[prop],
          requestChange(val){
            state.walletParams[prop] = val
            stateChanged()
          },
        }
      },
    }),
    state.error != null &&
      FormMessage({
        style: 'large',
        status: 'error',
        message: state.error,
      }),
    div({className: 'action-button-row'},
      Button({
        btnType: 'primary',
        disabled: !state.isValid(),
        title: 'Deploy wallet',
        isLoading: state.isDeploying,
      })
    )
  )
})

})
