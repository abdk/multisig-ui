define(function(require){

var config = require('config')
var {Stateful} = require('core')
var {executeData} = require('wallet')
var {signTransaction} = require('transfer')
var storage = require('browserStorage')
var FormMessage = require('widgets/formMessage')
var getErrorMessage = require('./getErrorMessage')
var TokenSelect = require('widgets/tokenSelect')
var FormGroup = require('widgets/formGroup')
var DataFormGroup = require('widgets/dataFormGroup')
var Button = require('widgets/button')
var AddressFormGroup = require('widgets/addressFormGroup')
var AccountSelect = require('widgets/accountSelect')
var AmountFormGroup = require('widgets/amountFormGroup')
var DecimalsAwareAmountFormGroup = require('widgets/decimalsAwareAmountFormGroup')
var {OfflineTransactionBlock, isValid} = require('ui/offlineTransactionBlock')
var Form = require('widgets/form')

var {div, a} = React.DOM

function CreateTransactionState({stateChanged}){
  return {
    tokens: storage.loadFromMainDB('tokens').map(t => Object.assign(t, {
      decimals: t.decimals && parseInt(t.decimals)
    })),
    wallets: storage.loadFromMainDB('walletBook'),
    params: {
      gasPrice: Web3.prototype.toBigNumber(config.DEFAULT_GAS_PRICE),
      gasLimit: config.DEFAULT_GAS_LIMIT,
    },

    isValid(){
      return true
        && this.wallet != null
        && this.to != null
        && this.amount != null
        && this.token != null
        && (
          this.token != 'ether' || this.data == null || this.data.isValid
        )
        && isValid(this.params)
    },

    setToken(token){
      this.token = token
      this.amount = null
      if(this.token != 'ether'){
        this.data = null
      }
      stateChanged()
    },
    
    getToken(){
      if(this.token == 'ether'){
        return this.token
      } else {
        return this.tokens
          .find(token => token.address == this.token)
      }
    },

    create(){
      this.error = null
      this.isLoading = true
      stateChanged()
      var tx = {
        to: this.wallet,
        gasLimit: this.params.gasLimit,
        gasPrice: this.params.gasPrice,
        nonce: this.params.nonce,
        chainId: this.params.chainId,
        data: executeData({
          token: this.token, 
          customData: this.token == 'ether' 
            ? this.data && this.data.value
            : null,
          to: this.to, 
          amount: this.amount,
        }),
      }
      return signTransaction(tx, this.params.signData)
        .then(signedTx => {
          this.isLoading = false
          console.log('signed', signedTx)
          saveAs(
            new Blob([signedTx]), 
            'transaction-' 
            + Web3.prototype.toChecksumAddress(this.wallet) 
            + '-'
            + new Date().toLocaleString()
          )
          stateChanged()
        })
        .catch(e => {
          this.isLoading = false
          this.error = getErrorMessage(e)
          stateChanged()
        })

    },
  }
}

return Stateful(CreateTransactionState, function({state, link}){
  return Form({onSubmit: state.create},
    FormGroup({
      labelMsg: 'Wallet',
      input: div({className: 'form-input-addon'},
        AccountSelect({
          accounts: state.wallets,
          valueLink: link('wallet'),
          placeholder: 'Wallet',
        }),
        a({href: '#wallets'}, 'Create wallet')
      )
    }),
    FormGroup({
      labelMsg: 'Token or Ether',
      input: TokenSelect({
        canSelectEther: true,
        valueLink: {
          value: state.token,
          requestChange: state.setToken,
        },
        tokens: state.tokens,
      })
    }),
    AddressFormGroup({
      name: 'eth-address',
      labelMsg: 'To',
      valueLink: link('to'),
    }),
    state.token == null
      ? AmountFormGroup({
          key: state.token,
          valueLink: link('amount'),
        })
      : DecimalsAwareAmountFormGroup({
          key: state.token,
          valueLink: link('amount'),
          token: state.getToken(),
        }),
    state.token == 'ether' && DataFormGroup({
      labelMsg: 'Data (optional)',
      valueLink: link('data'),
    }),
    OfflineTransactionBlock({valueLink: link('params')}),
    state.error && 
      FormMessage({
        message: state.error,
        status: 'error',
      }),
    div({className: 'action-button-row'},
      Button({
        btnType: 'primary',
        disabled: !state.isValid(),
        title: 'Sign and export transaction for Multisig Online',
        isLoading: state.isLoading,
      })
    )
  )
})

})
