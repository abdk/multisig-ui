define(function(require){

var {messages} = require('i18n');
var {div, span, table, tr, tbody, td, input, b, ul, li, label, button, i} = React.DOM;
var {connect} = require('core');
var Button = require('widgets/button');
var {toggleNodeAddress} = require('./nodeAddress');
var {loadNodeAddress, isNodeConfigured} = require('ethNode');

function InfoTable({network}){
  var info = state.nodeInfo.get();
  var data = [
    {
      name: messages.nodeInfo.networkName,
      value: network.name,
    },
    {
      name: messages.nodeInfo.peers,
      value: info.peerCount,
    },
    {
      name: messages.nodeInfo.blockNumber,
      value: info.block.number,
    },
    {
      name: messages.nodeInfo.sinceLastBlock,
      value: info.block.time != null
        ? Math.floor((new Date().getTime() - info.block.time) / 1000)
        : messages.nodeInfo.unknown,
    },
  ]
  if(info.syncing){
    var blocksLeftToSync = info.syncing.highestBlock - info.syncing.currentBlock
    var percentSynced = Math.round(
      (1 - blocksLeftToSync / (info.syncing.highestBlock - info.syncing.startingBlock))
      * 100
    )
    data.push({
      name: messages.nodeInfo.blocksLeftToSync,
      value: ''
        + blocksLeftToSync
        + ' ('
        + percentSynced
        + '% ' + messages.nodeInfo.completed + ')'
    })
  }
  return table(null, tbody(null,
    ...data.map(({name, value}) => (
      tr({className: 'name-value'},
        td({className: 'name'},
          name
        ),
        td({className: 'value'},
          value
        )
      )
    ))
  ))
}

var InfoTableTimeUpdated = React.createFactory(React.createClass({

  componentWillMount(){
    this.__intervalId = setInterval(() => this.forceUpdate(), 1000)
  },

  componentWillUnmount(){
    clearInterval(this.__intervalId)
  },

  render(){
    // TODO don't load on every render
    return InfoTable({network: loadNodeAddress()})
  },

}))

var NodeInfo = React.createFactory(
  connect('nodeInfo', (props) => {
    var isConfigured = isNodeConfigured()
    var info = isConfigured
      ? state.nodeInfo.get()
      : {isOnline: false}

    return div({className: 'nodeInfo-container'},
      button({className: 'nodeInfo', onClick: toggleNodeAddress},
        info.isOnline 
          ? info.syncing
            ? span({className: 'syncing'}, messages.nodeInfo.syncing)
            : span({className: 'online'}, messages.nodeInfo.online)
          :  span({className: 'offline'}, messages.nodeInfo.offline)
      ),
      isConfigured &&
        div({className: 'th-dropdown nodeInfo-content'},
          InfoTableTimeUpdated()
        )
    )
  })
)

return NodeInfo;

});
