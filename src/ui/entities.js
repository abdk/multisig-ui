define(function(require){

var {Stateful, dataChanged, getComponent} = require('core')
var storage = require('browserStorage')
var Card = require('widgets/card')
var Button = require('widgets/button')
var makeEntityDialogComponent = require('./entityDialog')

var {div, span} = React.DOM;

function capitalize(string) {
  return string.charAt(0).toUpperCase() + string.slice(1)
}

return function makeEntitiesComponent({entityName, storageKey, withDecimals}){
  
  var Dialog = makeEntityDialogComponent({entityName, withDecimals})

  function addEntity(entity){
    var entities = storage.loadFromMainDB(storageKey)
    entities.unshift(entity)
    storage.saveToMainDB(storageKey, entities)
    dataChanged(storageKey)
  }

  function editEntity(entity, nextEntity){
    var entities = storage.loadFromMainDB(storageKey)
    storage.saveToMainDB(storageKey, entities.map(t => 
      entity.address == t.address
      ? nextEntity
      : t
    ))
    dataChanged(storageKey)
  }

  function removeEntity(entity){
    var entities = storage.loadFromMainDB(storageKey)
    storage.saveToMainDB(storageKey, entities.filter(t => 
      t.address != entity.address
    ))
    dataChanged(storageKey)
  }

  class EntityEditState {

    constructor(entity, {onClose, stateChanged}){
      this.entity = entity
      this.onClose = onClose
      this.stateChanged = stateChanged
      this.onSubmit = this.onSubmit.bind(this)
      if(this.isEdit()){
        Object.assign(this, {
          address: entity.address,
          name: entity.name,
          decimals: entity.decimals,
        })
      }
    }

    link(prop){
      return {
        value: this[prop],
        requestChange: val => {
          this[prop] = val
          this.stateChanged()
        }
      }
    }

    isEdit(){
      return this.entity != null
    }

    canSubmit(){
      return this.address != null && this.name != '' && this.name != null &&
        this.isDecimalsValid()
    }

    getDecimals(){
      if(this.decimals == '' || this.decimals == null){
        return {isValid: true, value: null}
      } else {
        var value = Number(this.decimals)
        if(isNaN(value)){
          return {isValid: false}
        }else{
          return {isValid: true, value}
        }
      }
    }

    isDecimalsValid(){
      var {isValid, value} = this.getDecimals()
      return isValid && (
        value == null 
        ||
        value >= 0 && value % 1 == 0
      )
    }

    onSubmit(e){
      e.preventDefault()
      var nextEntity = {
        name: this.name, 
        address: this.address, 
        decimals: this.getDecimals().value,
      }
      if(this.isEdit()){
        editEntity(this.entity, nextEntity)
      }else{
        addEntity(nextEntity)
      }
      this.onClose()
      dataChanged(storageKey)
    }

  }


  function EntitiesState({stateChanged}){
    return {
      entities: storage.loadFromMainDB(storageKey),

      dataChanged: (function(){
        var dataChangedHandlers = {}
        dataChangedHandlers[storageKey] = function(){
          this.entities = storage.loadFromMainDB(storageKey)
          stateChanged()
        }
        return dataChangedHandlers
      })(),

      closeEditEntity(){
        delete this.entity
        stateChanged()
      },

      addEntity(){
        this.entity = new EntityEditState(null, {onClose: this.closeEditEntity, stateChanged})
        stateChanged()
      },

      edit(entity){
        this.entity = new EntityEditState(entity, {onClose: this.closeEditEntity, stateChanged})
        stateChanged()
      },

    }
  }

  var Entity = React.createFactory(function({state, entity}){
    return Card({
      title: entity.name,
      subTitle: Web3.prototype.toChecksumAddress(entity.address),
      options: [
        {
          name: 'Copy address',
          onClick: clipboard.copy.bind(null, 
            Web3.prototype.toChecksumAddress(entity.address)
          ),
        },
        {
          name: 'Edit',
          onClick: state.edit.bind(null, entity),
        },
        {
          name: 'Remove',
          onClick: removeEntity.bind(null, entity),
        },
      ]
    })
  })

  var List = Stateful(EntitiesState, function({state}){
    return div(null,
      Dialog({state: state.entity}),
      state.entities.length === 0
        ? div({className: 'box-nocontent'}, `You have no ${entityName}s yet`)
        : div(null,
            state.entities.map(entity => 
              Entity({state, entity, key: entity.address})
            )
          )
    )
  })

  var Header = span(null,
    span({style: {marginRight: '30px'}},
      capitalize(entityName) + 's'
    ),
    Button({
      title: 'Add ' + entityName,
      btnType: 'primary',
      type: 'button',
      onClick(){
        getComponent().addEntity()
      },
    })
  )

  return {List, Header}

}

})
