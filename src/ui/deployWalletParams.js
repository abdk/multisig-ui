define(function(require){

var Select = require('widgets/select')
var FormGroup = require('widgets/formGroup')
var FormMessage = require('widgets/formMessage')
var AddressList = require('widgets/addressList')
var TransactionSign = require('widgets/transactionSign')
var PasswordFormGroup = require('widgets/passwordFormGroup')
var AmountFormGroup = require('widgets/amountFormGroup')
var download = require('download')
var {div, form, a} = React.DOM

class DeployWalletParamsState {

  constructor(){
    this.walletType = 'tokenFriendly'
  }

  isValid(){
    return true 
      && this.owners != null
      && this.owners.length >= 1
      && this.signData != null
      && this.required != null
      && this.required <= this.effectiveOwners().length 
  }

  getReservedOwner(){
    return this.walletType == 'tokenFriendly'
      ? null
      : this.signData && this.signData.getAddress()
  }

  effectiveOwners(){
    return this.walletType == 'tokenFriendly'
      ? this.owners
      : [this.signData.getAddress()].concat(this.owners)
  }
  
  isShowSignerIsNotOwnerWarning(){
    return true
      && this.walletType == 'tokenFriendly' 
      && this.owners != null
      && this.signData != null
      && this.owners.indexOf(this.signData.getAddress()) == -1
  }

  args(){
    return this.walletType == 'standard' && this.daylimit != null
      ? [this.owners, this.required, Web3.prototype.toWei(this.daylimit, 'ether')]
      : [this.owners, this.required]
  }

}

var RequiredOwners = React.createFactory(React.createClass({

  getInitialState(){
    return {value: '', wasChanged: false}
  },

  onChange(e){
    var value = e.target.value
    this.setState({wasChanged: true, value}, () => {
      if(this.state.wasChanged && this.error() == null){
        this.props.valueLink.requestChange(this.number())
      } else {
        this.props.valueLink.requestChange(null)
      }
    })
  },

  number(){
    return Number(this.state.value)
  },

  error(){
    if(!this.state.wasChanged){
      return null
    }
    var value = this.state.value
    if(false 
      ||value.trim() == '' 
      || isNaN(this.number()) 
      || this.number() <= 0
      || Math.trunc(this.number()) != this.number()
    ){
      return 'Invalid number'
    }
    if(this.props.ownersCount != null && this.number() > this.props.ownersCount){
      return 'Cannot be greater than number of owners'
    }
    return null
  },

  render(props){
    return FormGroup({
      labelMsg: 'Number of owners required to confirm transaction',
      error: this.error(),
      input: React.DOM.input({
        className: 'form-control',
        type: 'number',
        min: 1,
        step: 1,
        value: this.state.value,
        onChange: this.onChange,
      })
    })
  }
}))

function DeployWalletParamsComponent({state, link}){
  return div(null,
    div({className: 'walletTypeBlock'},
      FormGroup({
        labelMsg: 'Wallet type',
        input: Select({
          simpleValue: true,
          clearable: false,
          options: [
            {value: 'tokenFriendly', label: 'Token friendly'},  
            {value: 'standard',      label: 'Standard'},  
          ],
          valueLink: link('walletType'),
        }),
        noError: true,
      }),
      div({className: 'download'},
        'Download',
        ...['sol', 'abi', 'bin', 'all'].map(assetType => 
          a({
            onClick: e => {
              e.preventDefault(); 
              download(state.walletType, assetType)
            }
          },
            assetType
          )
        )
      )
    ),
    state.walletType == 'standard' &&
      AmountFormGroup({
        labelMsg: 'Day limit (ether) (optional)',
        valueLink: link('daylimit'),
      }),
    TransactionSign({
      valueLink: link('signData'),
    }),
    AddressList({
      reservedAddress: state.getReservedOwner(),
      name: 'eth-address',
      labelMsg: 'Owner',
      valueLink: link('owners'),
    }),
    RequiredOwners({
      ownersCount: state.owners && state.signData && state.effectiveOwners().length,
      valueLink: link('required'),
    }),
    state.isShowSignerIsNotOwnerWarning() &&
      FormMessage({
        message: 'The signer is not among the contract owners',
        status: 'warning',
        style: 'large',
      })
  )
}

return {DeployWalletParamsState, DeployWalletParamsComponent}

})
