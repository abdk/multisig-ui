define(function(require){

var config = require('config')
var storage = require('browserStorage')
var {Stateful} = require('core')
var {confirmData} = require('wallet')
var {signTransaction} = require('transfer')
var getErrorMessage = require('./getErrorMessage')
var FormGroup = require('widgets/formGroup')
var FormMessage = require('widgets/formMessage')
var Button = require('widgets/button')
var AddressFormGroup = require('widgets/addressFormGroup')
var AccountSelect = require('widgets/accountSelect')
var {OfflineTransactionBlock, isValid} = require('ui/offlineTransactionBlock')
var trimInput = require('widgets/trimInput')
var {div, form, input, a} = React.DOM
var trimmedInput = trimInput(input)

function ConfirmTransactionState({stateChanged}){
  return {
    params: {
      gasPrice: Web3.prototype.toBigNumber(config.DEFAULT_GAS_PRICE),
      gasLimit: config.DEFAULT_GAS_LIMIT,
    },

    wallets: storage.loadFromMainDB('walletBook'),

    isValid(){
      return true
        && this.wallet != null
        && this.operation != null
        && isValid(this.params)
    },

    create(e){
      e.preventDefault()
      this.isLoading = true
      this.error = null
      stateChanged()
      var tx = {
        to: this.wallet,
        gasLimit: this.params.gasLimit,
        gasPrice: this.params.gasPrice,
        nonce: this.params.nonce,
        chainId: this.params.chainId,
        data: confirmData(this.operation),
      }
      signTransaction(tx, this.params.signData)
        .then(signedTx => {
          console.log('signed', signedTx)
          saveAs(
            new Blob([signedTx]), 
            'transaction-' 
            + Web3.prototype.toChecksumAddress(this.wallet) 
            + '-'
            + new Date().toLocaleString()
          )
          this.isLoading = false
          stateChanged()
        })
        .catch(e => {
          this.error = getErrorMessage(e)
          this.isLoading = false
          stateChanged()
        })

    },
  }
}

return Stateful(ConfirmTransactionState, function({state, link}){
  return form({onSubmit: state.create},
    FormGroup({
      labelMsg: 'Wallet',
      input: div({className: 'form-input-addon'},
        AccountSelect({
          accounts: state.wallets,
          valueLink: link('wallet'),
          placeholder: 'Wallet',
        }),
        a({href: '#wallets'}, 'Create wallet')
      )
    }),
    // TODO validate hash
    FormGroup({
      labelMsg: 'Operation hash',
      input: trimmedInput({
        className: 'form-control',
        type: 'text',
        valueLink: link('operation'),
      })
    }),
    OfflineTransactionBlock({valueLink: link('params')}),
    state.error != null &&
      FormMessage({
        status: 'error',
        message: state.error,
      }),
    div({className: 'action-button-row'},
      Button({
        btnType: 'primary',
        disabled: !state.isValid(),
        title: 'Sign and export transaction for Multisig Online',
        isLoading: state.isLoading,
      })
    )
  )
})

})
