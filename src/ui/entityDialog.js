define(function(require){

var {Stateful} = require('core')
var {messages} = require('i18n')
var storage = require('browserStorage')
var {form, div, span, button, label, i, input} = React.DOM;
var trimInput = require('widgets/trimInput')
var AddressFormGroup = require('widgets/addressFormGroup')
var Modal = require('widgets/modal')
var FormGroup = require('widgets/formGroup')
var trimmedInput = trimInput(input)

return function makeEntityDialogComponent({entityName, withDecimals}){

  return React.createFactory(function({state}){
    var s = state
    if(s == null){
      return null
    }
    var buttons = [
      {
        type: 'button',
        btnType: 'default',
        onClick: state.onClose,
        title: messages.form.cancel,
      },
      {
        disabled: !s.canSubmit(),
        btnType: 'success',
        title: messages.form.save
      },
    ]
    return form({onSubmit: s.onSubmit},
      Modal({
        title: s.isEdit()
          ? 'Edit ' + entityName
          : 'Add '  + entityName
        ,
        onClose: state.onClose,
        buttons
      },
        FormGroup({
          labelMsg: 'Name',
          input: trimmedInput({
            className: 'form-control',
            valueLink: state.link('name')
          })
        }),
        AddressFormGroup({
          labelMsg: 'Address',
          valueLink: state.link('address'),
        }),
        withDecimals &&
          FormGroup({
            labelMsg: 'Decimals (optional)',
            input: input({
              type: 'text',
              className: 'form-control', 
              min: 0,
              step: 1,
              defaultValue: state.decimals,
              onChange: e => state.link('decimals').requestChange(e.target.value),
            }),
            error: state.isDecimalsValid() ? null : 'Invalid number',
          })
      )
    )
  })

}

})
