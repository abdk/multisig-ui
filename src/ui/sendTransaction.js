define(function(require){

var {Stateful} = require('core')
var {openFile, readFile} = require('utils/file')
var {sendRawTransaction} = require('wallet')
var getErrorMessage = require('./getErrorMessage')
var FormMessage = require('widgets/formMessage')
var Button = require('widgets/button')

var {div, form, pre} = React.DOM

function SendTransactionState({stateChanged}){
  return {
    // TODO check tx file is BASE-64
    importTransaction(){
      this.error = null
      this.isLoading = true
      stateChanged()
      openFile()
        .then(files => files[0])
        .then(readFile)
        .then(rawTx => {
          if(rawTx.indexOf('0x') != 0){
            rawTx = '0x' + rawTx
          }
          return rawTx
        })
        .then(sendRawTransaction)
        .then(tx => {
          if(tx.to == null){
            window.location.hash = 'wallets'
          } else {
            window.location.hash = 'transactions'
          }
        })
        .catch(e => {
          this.isLoading = false
          this.error = getErrorMessage(e)
          stateChanged()
          throw e
        })
    },
  }
}

return Stateful(SendTransactionState, function({state, link}){
  return div(null,
    Button({
      type: 'button',
      btnType: 'primary',
      title: 'Import transaction',
      onClick: state.importTransaction, }),
    state.error != null &&
      FormMessage({
        status: 'error',
        message: state.error,
        style: 'large',
      })
  )
})

})
