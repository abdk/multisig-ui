define(function(require){

var {Stateful} = require('core')
var storage = require('browserStorage')
var {fetchOperationStatus} = require('wallet')
var FormMessage = require('widgets/formMessage')
var FormGroup = require('widgets/formGroup')
var AddressFormGroup = require('widgets/addressFormGroup')
var AddressInputOrSelectFormGroup = require('widgets/addressInputOrSelectFormGroup')
var Account = require('widgets/account')
var Button = require('widgets/button')
var Table = require('widgets/table')
var getErrorMessage = require('./getErrorMessage')
var {div, form, input, textarea} = React.DOM
var trimInput = require('widgets/trimInput')
var trimmedInput = trimInput(input)

function VerifyTransactionState({stateChanged}){
  return {

    wallets: storage.load('wallets')
      .filter(w => w.address != null)
      .map(w => w.address),

    wallet: sessionStorage.verifyTransaction_wallet,

    operation: sessionStorage.verifyTransaction_operation,

    linkToSessionStorage(param){
      return {
        value: this[param],
        requestChange: value => {
          this[param] = value
          if(value == null){
            delete sessionStorage['verifyTransaction_' + param]
          } else {
            sessionStorage['verifyTransaction_' + param] = value
          }
          stateChanged()
        },
      }
    },

    isValid(){
      return this.wallet != null && this.operation != null
    },

    verify(e){
      e.preventDefault()
      this.status = null
      this.error = null
      this.isLoading = true
      stateChanged()
      fetchOperationStatus(this.wallet, this.operation)
        .then(status => {
          this.status = status
          this.isLoading = false
          stateChanged()
        })
        .catch(e => {
          this.error = getErrorMessage(e)
          this.isLoading = false
          stateChanged()
        })
    },

    message(){
      if(this.status.status == 'notfound'){
        return {status: 'error', message: 'Operation is not found'}
      } else if(this.status.status == 'completed'){
        return {status: 'success', message: 'Operation is already confirmed'}
      } else if(this.status.status == 'confirming'){
        return {status: 'success', message: 'Operation is being confirmed'}
      } else {
        throw new Error('Unknown status: ' + this.status.status)
      }
    },

    isUnparsabaleDataPresent(){
      return this.status.status == 'confirming' && !this.status.params.isValid
    },

    isConfirming(){
      return true
        && this.status != null 
        && this.status.status != 'notfound' 
    },

    isShowParams(){
      return true
        && this.status != null 
        && this.status.status == 'confirming' 
        && this.status.params.isValid
    },

  }
}

function useDecimals(value, decimals = 0){
  return value.shift(Web3.prototype.toBigNumber(-1).times(decimals))
}

function tableRows(state){
  var result = [
    ['Type', state.status.params.name],
    ['Token', state.status.params.token == null
              ? 'Ether'
              : Account({address: state.status.params.token}),]
  ]
  if(state.status.params.to != null){
    result.push(['To',    Account({address: state.status.params.to})])
  }
  result.push(
    ['Value', 
      state.status.params.token == null
      ? Web3.prototype.fromWei(state.status.params.value, 'ether')
        .toFormat() + ' Ether'
      : useDecimals(
          state.status.params.value, 
          state.status.params.decimals
        ).toFormat() 
        + ' (decimals: ' 
        + (state.status.params.decimals == null
            ? 'Unknown'
            : state.status.params.decimals
          )
        + ')'
    ]
  )
  return result
}

return Stateful(VerifyTransactionState, function({state, link}){
  return form({onSubmit: state.verify},
    state.wallets.length == 0
      ? AddressFormGroup({
          name: 'eth-wallet',
          autoFocus: true,
          valueLink: state.linkToSessionStorage('wallet'),
          labelMsg: 'Wallet',
        })
      : AddressInputOrSelectFormGroup({
          name: 'eth-wallet',
          addresses: state.wallets,
          valueLink: state.linkToSessionStorage('wallet'),
          labelMsg: 'Wallet',
        }),
    // TODO validate hash
    FormGroup({
      labelMsg: 'Operation hash',
      input: trimmedInput({
        className: 'form-control',
        type: 'text',
        valueLink: state.linkToSessionStorage('operation'),
      })
    }),
    div({className: 'action-button-row'},
      Button({
        btnType: 'primary',
        isLoading: state.isLoading,
        disabled: !state.isValid(),
        title: 'Get details',
      })
    ),
    state.isShowParams() &&
      Table({
        style: {marginTop: '20px'},
        className: 'table',
        rows: tableRows(state),
      }),
    state.error != null &&
      FormMessage({
        status: 'error',
        message: state.error,
        style: 'large',
      }),
    state.status != null && div(null,
      FormMessage({
        status: state.message().status,
        message: state.message().message,
        style: 'large',
      }),
      state.isUnparsabaleDataPresent() && div({style: {marginBottom: '50px'}},
        FormMessage({
          status: 'warning',
          message: 'Warning: data present:',
          style: 'large',
        }),
        textarea(null, 
          state.status.params.data
        )
      )
    ),
    state.isConfirming() && div(null,
      div(null,
        'Confirmations: ', 
        state.status.confirmations.length,
        ' of ',
        state.status.required
      ),
      div(null,
        state.status.confirmations.map(address => 
          Account({address, key: address})
        )
      )
    )
  )
})

})
