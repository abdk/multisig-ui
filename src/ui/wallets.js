define(function(require){

var {Stateful} = require('core')
var storage= require('browserStorage')
var Account= require('widgets/account')
var copy = require('widgets/copy')
var {div, input, i, span, ul, li, a} = React.DOM
var etherscanLink = require('widgets/etherscanLink')

function WalletsState({stateChanged}){
  return {
    wallets: storage.load('wallets'),

    dataChanged: {
      wallets(){
        console.log('wallets updated')
        this.wallets = storage.load('wallets')
        stateChanged()
      }
    },
  }
}

return Stateful(WalletsState, function({state, link}){
  return state.wallets.length == 0
    ? div({className: 'nocontent'}, 
      'You have not deployed any wallets yet'
    )
    : div(null,
      state.wallets.map(wallet => 
        div({className: 'card transaction', key: wallet.transactionHash},
          div({className: 'left-col'},
            div({className: 'address'},
              wallet.address 
                && copy({value: Web3.prototype.toChecksumAddress(wallet.address)},
                  etherscanLink({address: wallet.address},
                    Account({address: wallet.address})
                  )
                )
                || 'Waiting for contract address'
            ),
            div({className: 'date'},
              wallet.date.toLocaleString()
            )
          ),
          wallet.owners != null &&
            div({className: 'data-block'},
              wallet.daylimit != null &&
                div({className: 'caption'},
                  "Daylimit: " + wallet.daylimit.toFormat() + ' Ether'
                ),
              div({className: 'owners'},
                div({className: 'caption'},
                  'Owners (' + wallet.required + ' required):'
                ),
                wallet.owners.map(address => Account({key: address, address}))
              )
            ),
          div({className: 'status'},
            wallet.address == null
              ? div(null,
                  'Pending', 
                  i({className: 'fa fa-refresh fa-spin'})
                )
              : div({className: 'deployed'},
                  'Deployed'
                )
          )
        )
      )
    )
})

})
