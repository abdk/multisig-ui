define(function(require){

var {Stateful} = require('core')
var storage = require('browserStorage')
var copy = require('widgets/copy')
var Account = require('widgets/account')
var etherscanLink = require('widgets/etherscanLink')
var {div, span, i} = React.DOM

function TransactionState({stateChanged}){
  return {

    transactions: storage.load('transactions'),

    dataChanged: {
      transactions(){
        this.transactions = storage.load('transactions'),
        stateChanged()
      }
    },

  }
}

return Stateful(TransactionState, function({state}){
  return div(null,
    state.transactions.length == 0
    ? div({className: 'box-nocontent'}, 'You have no transactions yet')
    : state.transactions.map(tx => div({className: 'card transaction', key: tx.transaction.hash}, 
          div({className: 'left-col'},
            'Hash: ',
            div({className: 'address'},
              copy({value: tx.transaction.hash},
                etherscanLink({className: 'hash', address: tx.transaction.hash}, 
                  tx.transaction.hash
                )
              )
            ),
            tx.metadata.isSingleTransact
              ? span(null,
                  'Operation completed (under day limit)'
                )
              : tx.metadata.operation && div(null,
                  span({style: {whiteSpace: 'nowrap'}},
                    'Operation to confirm by other owners: '
                  ),
                  div({className: 'address'},
                    copy({value: tx.metadata.operation},
                      div({className: 'hash'}, tx.metadata.operation)
                    )
                  )
                ),
              div({className: 'date'},
                tx.metadata.date.toLocaleString()
              )
          ),
          (tx.metadata.type == 'confirm' || tx.metadata.isValid)
            ? div({className: 'data-block'},
                div(null, 
                  'Type: ', tx.metadata.type + 
                  (
                    tx.metadata.name == null
                    ? ''
                    : (' ' + tx.metadata.name)
                  )
                ),
                tx.metadata.to != null &&
                  div(null, 'To: ', Account({address: tx.metadata.to})),
                tx.metadata.type == 'execute' &&
                  div(null, 'Token: ', tx.metadata.token == null 
                    ? 'Ether'
                    : Account({address: tx.metadata.token})),
                tx.metadata.type == 'execute' &&
                  div(null, 'Value:', tx.metadata.token == null
                    ? Web3.prototype.fromWei(tx.metadata.value, 'ether').toFormat()
                    : span(null, tx.metadata.value.toFormat(), ' (decimals: unknown)')
                  )
              )
            : div({className: 'data-block'},
                div({style: {color: 'red'}}, 
                  'Could not parse transaction data'
                )
              ),
          div({className: 'status'},
            tx.receipt == null
              ? div(null,
                  'Pending', 
                  i({className: 'fa fa-refresh fa-spin'})
                )
              : div({className: 'deployed'},
                  'Confirmed'
                )
          )
      ))
  )
})

})
