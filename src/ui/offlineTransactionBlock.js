define(function(require){

var {i, div, button, input, label} = React.DOM

var numberInput = require('widgets/numberInput')
var bigNumberInput = require('widgets/numberInput')
var FormGroup = require('widgets/formGroup')
var TransactionSign = require('widgets/transactionSign')
var PasswordFormGroup = require('widgets/passwordFormGroup')

function isValid(params){
  return true
    && params.signData != null
    && params.nonce != null
    && params.chainId != null
    && params.gasPrice != null
    && params.gasLimit != null
}

var NETWORKS = [
  {id: 'mainnet', name: 'Main net'},
  {id: 'ropsten', name: 'Testnet (Ropsten)'},
  {id: 'custom' , name: 'Custom'},
]

var Network = React.createFactory(React.createClass({

  getInitialState(){
    return {}
  },

  onChange(value){
    this.setState({value})
    var chainId = {
      mainnet: 1,
      ropsten: 3,
      custom: value.chainId,
    }[value.network]
    this.props.valueLink.requestChange(chainId)
  },

  render(){
    var value = this.state.value
    return div(null,
      NETWORKS.map(({id, name}) => 
        div({key: id, className: 'radio network-option'},
          label(null,
            input({
              type: 'radio', 
              id: 'network', 
              checked: (value && value.network) == id, onChange: () => {
                this.onChange({network: id})
              },
            }),
            name
          ), 
          (id == 'custom' && (value && value.network) == 'custom') &&
            div({className: 'chainID-input'},
              'Chain ID',
              numberInput({
                className: 'form-control',
                min: 0,
                step: 1,
                valueLink: {
                  value: value && value.chainId,
                  requestChange: (chainId) => {
                    this.onChange({
                      network: 'custom',
                      chainId,
                    })
                  }
                }
              })
            )
        )
      )
    )
  }
}))

function linkInto(link, prop){
  return {
    value: link.value[prop],
    requestChange(val){
      var nextValue = _.clone(link.value)
      nextValue[prop] = val
      link.requestChange(nextValue)
    }
  }
}

function toGwei(valueLink){
  return {
    value: Web3.prototype.fromWei(valueLink.value, 'gwei'),
    requestChange(value){
      valueLink.requestChange(
        Web3.prototype.toWei(
          Web3.prototype.toBigNumber(value), 
          'gwei'
        )
      )
    },
  }
}

var OfflineTransactionBlock = React.createFactory(function(props){
  return div(null,
    !props.omitTransactionSign &&
      TransactionSign({valueLink: linkInto(props.valueLink, 'signData')}),
    FormGroup({
      labelMsg: 'Nonce',
      input: numberInput({
        className: 'form-control',
        valueLink: linkInto(props.valueLink, 'nonce'),
      }),
      noError: true,
    }),
    FormGroup({
      labelMsg: 'Gas Limit',
      status: 'warning',
      error: 'increase this value for token contracts with expensive transfers',
      input: numberInput({
        className: 'form-control',
        valueLink: linkInto(props.valueLink, 'gasLimit'),
      }),
    }),
    FormGroup({
      labelMsg: 'Gas Price (in Gwei)',
      input: bigNumberInput({
        className: 'form-control',
        valueLink: toGwei(linkInto(props.valueLink, 'gasPrice')),
        noError: true,
      }),
    }),
    FormGroup({
      labelMsg: 'Network',
      input: Network({
        valueLink: linkInto(props.valueLink, 'chainId')
      }),
    })
  )
})

return {
  OfflineTransactionBlock,
  isValid,
}

})
