define(function(require){

var storage = require('browserStorage')
var config = require('config')
var {deploymentData} = require('wallet')
var {signTransaction} = require('transfer')
var {DeployWalletParamsState, DeployWalletParamsComponent} = 
  require('ui/deployWalletParams')
var getErrorMessage = require('./getErrorMessage')
var {Stateful} = require('core')
var {OfflineTransactionBlock} = require('ui/offlineTransactionBlock')
var Button = require('widgets/button')
var FormMessage = require('widgets/formMessage')
var Form = require('widgets/form')
var {div, a} = React.DOM

function getInitialState({stateChanged}){
  return {
    
    walletParams: new DeployWalletParamsState(),

    offlineParams: {
      gasPrice: Web3.prototype.toBigNumber(config.DEFAULT_GAS_PRICE),
      gasLimit: 2000000//config.DEFAULT_GAS_LIMIT,
    },

    isValid(){
      return this.walletParams.isValid()
        && this.offlineParams.nonce != null
        && this.offlineParams.chainId != null
        && this.offlineParams.gasPrice != null
        && this.offlineParams.gasLimit != null
    },

    createTransaction(){
      this.isDeploying = true
      stateChanged()
      signTransaction({
        chainId: this.offlineParams.chainId,
        gasPrice: this.offlineParams.gasPrice,
        gasLimit: this.offlineParams.gasLimit,
        nonce: this.offlineParams.nonce,
        data: deploymentData(this.walletParams.walletType, ...this.walletParams.args()),
      }, this.walletParams.signData)
      .then(tx => {
        this.isDeploying = false
        stateChanged()
        saveAs(
          new Blob([tx]), 
          'transaction-deployWallet-' 
          + '-'
          + new Date().toLocaleString()
        )
      }).catch(e => {
        this.error = getErrorMessage(e)
        this.isDeploying = false
        stateChanged()
        throw e
      })
    },

  }
}

return Stateful(getInitialState, function({state, link, stateChanged}){
  return Form({onSubmit: state.createTransaction},
    DeployWalletParamsComponent({
      state: state.walletParams,
      link(prop){
        return {
          value: state.walletParams[prop],
          requestChange(val){
            state.walletParams[prop] = val
            stateChanged()
          },
        }
      },
    }),
    OfflineTransactionBlock({
      omitTransactionSign: true,
      valueLink: link('offlineParams'),
    }),
    state.error != null &&
      FormMessage({
        style: 'large',
        status: 'error',
        message: state.error,
      }),
    div({className: 'action-button-row'},
      Button({
        btnType: 'primary',
        disabled: !state.isValid(),
        title: 'Deploy wallet',
        isLoading: state.isDeploying,
      })
    )
  )
})

})
