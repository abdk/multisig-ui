define(function(require){

var {Stateful} = require('core')
var storage = require('browserStorage')
var AddressFormGroup = require('widgets/addressFormGroup')
var Account = require('widgets/account')
var {callWeb3} = require('utils/web3')
var {promisedProperties} = require('utils')
var copy = require('widgets/copy')
var FormMessage = require('widgets/formMessage')
var LedgerAddressSelect = require('widgets/ledgerAddressSelect')
var getErrorMessage = require('./getErrorMessage')
var {div, table, thead, tbody, tr, th, td, caption, a} = React.DOM

function fetchNonce(address){
  return callWeb3(web3.eth.getTransactionCount)(address)
}

function GetNonceState({stateChanged}){

  function NonceResult(address){
    var result = {
      isLoading: true
    }
    fetchNonce(address)
      .then(nonce => {
        result.nonce = nonce
        result.isLoading = false
        stateChanged()
      })
      .catch(e => {
        result.error = getErrorMessage(e)
        result.isLoading = false
        stateChanged()
        throw e
      })
    return result
  }

  var result

  var recent = storage.load('recentAddresses')
  var hasRecentAddresses = recent.length != 0
  if(recent.length != 0){
    Promise.all(recent.map(address => 
      promisedProperties({
        address,
        nonce: fetchNonce(address),
      })
    ))
    .then(recent => {
      result.recent = recent
      stateChanged()
    })
  }

  return result = {

    isGetLedgerAddress: false,

    hasRecentAddresses,

    setAddress(address){
      this.address = address
      if(address != null){
        this.addToRecentAddresses(address)
        this.addressNonceResult = NonceResult(address)
      }
      stateChanged()
    },

    showLedgetWalletAddress(){
      this.isGetLedgerAddress = true
      stateChanged()
    },

    setLedgerAddress({address}){
      this.ledgerAddress = address
      this.ledgerNonceResult = NonceResult(address)
      stateChanged()
    },

    addToRecentAddresses(address){
      var recent = storage.load('recentAddresses')
      var exists = recent.find(addr => addr == address) != null
      if(!exists){
        recent.unshift(address)
        storage.save('recentAddresses', recent)
      }
    },
  }
}

function NonceMessage(nonceResult){
  var {status, message} = (function(){
    return this.isLoading
    ? {status: 'loading', message: 'Loading nonce'}
    : nonceResult.error != null
      ? {status: 'error', message: nonceResult.error}
      : {
        status: 'success', 
        message: copy({isInline: true, value: nonceResult.nonce && nonceResult.nonce.toString()},
          'Nonce is: ' + nonceResult.nonce
        )
      }
  })()
  return FormMessage({status, message})
}

return Stateful(GetNonceState, function({link, state}){
  return div(null,
    AddressFormGroup({
      autoFocus: true,
      labelMsg: 'Address',
      valueLink: {
        value: state.address,
        requestChange: state.setAddress,
      },
    }),
    state.address != null && NonceMessage(state.addressNonceResult),
    state.hasRecentAddresses && (
      state.recent == null 
      ? FormMessage({message: 'Loading recent addresess', status: 'loading'})
      : table({className: 'table'}, 
          caption(null, 'Recent addresses'),
          thead(null, 
            tr(null,
              th(null, 'Address'),
              th(null, 'Nonce')
            )
          ),
          tbody(null,
            state.recent.map(({address, nonce}) =>
              tr(null,
                td(null, Account({address})),
                td(null,
                  copy({isInline: true, value: nonce.toString()},
                    'Nonce is: ' + nonce
                  )
                )
              )
            )
          )
        )
    ),
    state.isGetLedgerAddress
      ? div(null,
          LedgerAddressSelect({onChange: state.setLedgerAddress}),
          state.ledgerAddress != null &&
            NonceMessage(state.ledgerNonceResult)
        )
      : a({onClick: state.showLedgetWalletAddress}, 
          'Get nonce for Ledger Wallet address'
        )
  )
})

})
