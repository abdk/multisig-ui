define(function(require){

var core = require('core')
var {messages} = require('i18n');
var {label, h2, a, div, span, input, b, ul, button, form} = React.DOM;
var {link, stateChanged, connect} = require('core');
var initEthNode = require('initEthNode');
var {
  loadNodeConfiguration,
  saveNodeToRecents, 
  NODES_CATALOG,
  saveCatalogNode,
  fetchNetworkInfo,
  getChainId,
} = require('ethNode');
var Modal = require('widgets/modal');
var FormGroup = require('widgets/formGroup');
var numberInput = require('widgets/numberInput');
var Select = require('widgets/select');
var Tabs = require('widgets/tabs');

function toggleNodeAddress(){
  state.ui.isShowNodeAddress = !state.ui.isShowNodeAddress
  stateChanged('nodeAddress');
}

function initUIState(){
  state.ui.nodeAddress = new NodeAddressState()
}

class NodeAddressState {

  /*
    States for this form:

    Initial states:

    - EMPTY - user opens app for the first time, node is not configured
    - OFFLINE - node is configured, but is offline when app starts (TODO -
      remove this state after implementing offline mode)
    - EDIT - node is configured, but user wants to connect to another node

    Transitions:

    - EMPTY -> CONFIGURE_CHAIN_ID - user enters URL and it is valid private
      node, must configure CHAIN_ID
    - EMPTY -> OFFLINE - user enters URL and it is not a node
    - EMPTY -> FINISH - user enters URL and it is valid known node, no need to
      configure chain id
    - CONFIGURE_CHAIN_ID -> FINISH
  */

  constructor(){
    this.conf = loadNodeConfiguration()
    this.currentChainId = this.conf.isNodeConfigured
      ? getChainId()
      : -1
    if(this.conf.isNodeConfigured){
      if(this.conf.nodeId != null){
        this.applyNode(this.conf.nodeId)
        this.isCustomNode = true
      } else {
        this.catalogNodeId = this.conf.catalogNodeId
        this.url = ''
        this.isCustomNode = false
      }
    } else {
      this.url = ''
      this.isCustomNode = false
    }

    this.isChecking = false
    this.isShowOfflineMessage = this.conf.isNodeConfigured 
      && !state.nodeInfo.get().isOnline
      
    this.isURLOk = this.conf.isNodeConfigured && this.conf.nodeId != null && 
      state.nodeInfo.get().isOnline

    //bind
    this.setCatalogNodeId = this.setCatalogNodeId.bind(this)
    this.onSelectRecentNode = this.onSelectRecentNode.bind(this)
    this.stateChanged = () => stateChanged('nodeAddress')
    this.onSubmit = this.onSubmit.bind(this)
    this.changeCustomNode = this.changeCustomNode.bind(this)
  }
  
  changeCustomNode(isCustomNode){
    this.isCustomNode = isCustomNode
    this.stateChanged()
  }

  applyNode(nodeId){
    var network = _.findWhere(this.conf.recentNodes, {id: nodeId})
    var {chainId, url} = network
    this.network = network
    this.nodeId = nodeId
    this.chainId = chainId
    if(this.chainIdInput != null){
      this.chainIdInput.setValue(chainId)
    }
    this.url = url
  }

  isShowRecentNodes(){
    return this.conf.isNodeConfigured && this.conf.recentNodes.length != 0
  }

  onSelectRecentNode(id){
    this.nodeId = id
    this.isShowOfflineMessage = false;
    if(id == null){
      this.url = ''
      this.chainId = null
      this.network = null
    } else {
      this.applyNode(id)
    }
    this.stateChanged();
  }

  recentNodeOptions(){
    return this.conf.recentNodes.map(node => ({
      value: node.id, label: node.url  
    }))
  }

  nodeAddressLink(){
    return link(this, 'url')
      .andThen((val) => {
        this.isShowOfflineMessage = false;
        this.nodeId = null
        this.isURLOk = false;
        this.stateChanged();
      })
      .andThen(this.stateChanged)
  }

  chainIdLink(){
    return link(this, 'chainId').andThen(this.stateChanged)
  }

  setCatalogNodeId(id){
    console.log('setCatalogNodeId', id)
    this.catalogNodeId = id
    this.stateChanged()
  }

  isChainIdRequired(){
    return this.isURLOk && this.network.type == 'privatenet'
  }

  save(){
    var address = {
      url: this.url,
      type: this.network.type,
      name: this.network.name,
      firstBlockHash: this.network.firstBlockHash,
      chainId: this.network.type == 'privatenet' 
        ? this.chainId
        : this.network.chainId
    }
    saveNodeToRecents(address);
    this.onAfterSave()
  }

  onAfterSave(){
    initEthNode()
    toggleNodeAddress()
    if(getChainId() != this.currentChainId){
      window.location.reload()
    }
  }

  onSubmit(e){
    e.preventDefault()
    if(!this.isCustomNode){
      saveCatalogNode(this.catalogNodeId)
      this.onAfterSave()
    } else {
      this.isChecking = true;
      this.stateChanged()
      Promise.resolve().then(() => {
        if(this.isURLOk){
          return this.save();
        } else {
          var url = this.url;
          return fetchNetworkInfo(url).then(({isOnline, network}) => {
            this.isShowOfflineMessage = !isOnline
            if(isOnline){
              this.isURLOk = true;
              this.network = network;
              if(!this.isChainIdRequired()){
                return this.save();
              }else{
                setTimeout(() => {
                  ReactDOM.findDOMNode(this.chainIdInput).focus()
                }, 100)
              }
            }
          })
        }
      }).then(() => {
        this.isChecking = false;
        this.stateChanged()
      })
    }
  }

  isSubmitDisabled(){
    if(this.isCustomNode){
      if(!this.isURLOk){
        return this.url.trim() == ''
      } else {
        return this.isChainIdRequired() && this.chainId == null
      }
    } else {
      return this.catalogNodeId == null ||
        this.conf.catalogNodeId == this.catalogNodeId
    }
  }

  setChainIdInput(chainIdInput){
    this.chainIdInput = chainIdInput
  }

}

function focusInput(input){
  if(input != null){
    input.select();
    input.focus();
  }
}

function nodeRenderer(recentNodes, {value}){
  var node = _.findWhere(recentNodes, {id: value})
  var label = node.url + " " + node.name
  if(node.type == 'privatenet'){
    label += ' chainId: ' + node.chainId
  }
  return label
}

function DefaultNodes(s){
  return div(null,
    NODES_CATALOG.map(node => 
      div({className: 'radio', key: node.id},
        label(null,
          input({
            type: 'radio', 
            name: 'defaultNodes',
            checked: s.catalogNodeId == node.id,
            onChange: s.setCatalogNodeId.bind(null, node.id),
          }),
          node.name
        )
      )
    )
  )
}

function CustomNode(s){
  return div(null, 
    s.isShowRecentNodes() && FormGroup({
      labelMsg: messages.nodeSettings.recentNodes,
      input: Select({
        simpleValue: true,
        placeholder: messages.nodeSettings.selectRecentNode,
        value: s.nodeId,
        searchable: false,
        clearable: false,
        onChange: s.onSelectRecentNode,
        options: s.recentNodeOptions(),
        optionRenderer: nodeRenderer.bind(null, s.conf.recentNodes),
        valueRenderer: nodeRenderer.bind(null, s.conf.recentNodes),
      }),
      noError: true
    }),
    FormGroup({
      labelMsg: messages.nodeSettings.nodeURL,
      input: input({
        type: 'url',
        className: 'form-control', 
        valueLink: s.nodeAddressLink(),
        autoFocus: !s.conf.isNodeConfigured
      }),
      error: s.isShowOfflineMessage 
        ? messages.nodeSettings.couldNotConnectToNode
        : null
    }),
    FormGroup({
      invisible: !s.isChainIdRequired(),
      labelMsg: messages.nodeSettings.chainId,
      input: numberInput({
        step: 1,
        min: 0,
        className: 'form-control', 
        valueLink: s.chainIdLink(),
        ref(i){ s.setChainIdInput(i) }
      }),
    })
  )
}

var NodeAddressDialog = React.createFactory(React.createClass({

  componentWillMount(){
    initUIState()
  },

  render(){
    var s = state.ui.nodeAddress;
    var buttons = []
    if(s.conf.isNodeConfigured){
      buttons.push({
        type: 'button',
        title: messages.form.cancel,
        btnType: 'default',
        onClick: toggleNodeAddress
      })
    }
    buttons.push({
      type: 'submit',
      title: messages.form.apply,
      btnType: 'primary',
      isLoading: s.isChecking,
      disabled: s.isSubmitDisabled()
    })
    return form({
      className: 'nodeAddress', 
      onSubmit: s.onSubmit,
      // TODO portal for modals
      style: {marginBottom: '0px'}
    },
      Modal({
        title: messages.nodeSettings.nodeSettings,
        onClose: s.conf.isNodeConfigured ? toggleNodeAddress : null,
        buttons
      },
        Tabs({
          activeTabLink: {
            value: s.isCustomNode ? 'custom' : 'default',
            requestChange(value){
              s.changeCustomNode(value == 'custom')
            }
          },
          tabs: [
            {
              id: 'default',
              header: 'Default nodes',
              content: () => DefaultNodes(s),
            },
            {
              id: 'custom',
              header: 'Custom node',
              content: () => CustomNode(s),
            },
          ]
        })
      )
    )
  }
}))

var NodeAddress = React.createFactory(connect('nodeAddress', function(){
  if(state.ui.isShowNodeAddress){
    return NodeAddressDialog()
  } else {
    return null
  }
}))

return {NodeAddress, toggleNodeAddress}

});
