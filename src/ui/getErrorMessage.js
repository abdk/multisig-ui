define(function(require){

return function(e){
  if(e.isWeb3Error){
    return e.message
  }
  if(e.isInvalidPassword){
    return e.message
  }
  if(e.errorCode != null){
    return 'Could not sign transaction'
  }
  return 'Unknown error'
}

})
