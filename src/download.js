define(function(require){

var wallets = {
  standard: {
    sol: require('./wallet/Wallet.sol'),
    abi: require('./wallet/Wallet.abi'),
    bin: require('./wallet/Wallet.bin'),
  },
  tokenFriendly: {
    sol: require('./wallet/TokenFriendlyWallet.sol'),
    abi: require('./wallet/TokenFriendlyWallet.abi'),
    bin: require('./wallet/TokenFriendlyWallet.bin'),
  },
}

function fileContent(walletType, assetType){
  var content = wallets[walletType][assetType]
  if(assetType == 'abi'){
    content = JSON.stringify(content, null, 2)
  }
  if(assetType == 'all'){
    var zip = new JSZip()
    var root = zip.folder(fileName(walletType));
    ['sol', 'abi', 'bin'].forEach(assetType => 
      root.file(fullFileName(walletType, assetType), fileContent(walletType, assetType))
    )
    return zip.generateAsync({type:"blob"})
  }
  return content
}

function fileName(walletType){
  return {
    standard: 'Wallet',
    tokenFriendly: 'TokenFriendlyWallet',
  }[walletType]
}

function fullFileName(walletType, assetType){
  if(assetType == 'all'){
    assetType = 'zip'
  }
  return fileName(walletType) + '.' + assetType
}

return function(walletType, assetType){
  Promise.resolve()
  .then(() => fileContent(walletType, assetType))
  .then(content => 
    saveAs(
      new Blob([content]), 
      fullFileName(walletType, assetType)
    )
  )
}

})
