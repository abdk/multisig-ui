define(function(){

return {

  nodeInfo: {
    online: {
      en: 'online',
      ru: 'в сети',
    },
    offline: {
      en: 'offline',
      ru: 'не в сети',
    },
    syncing: {
      en: 'out of sync',
      ru: 'синхронизируется',
    },
    peers: {
      en: 'Peers',
      ru: 'Пиры',
    },
    sinceLastBlock: {
      en: 'Sec since last block',
      ru: 'Секунд со времени последнего блока',
    },
    blocksLeftToSync: {
      en: 'Blocks left to sync',
      ru: 'Осталось синхронизировать блоков',
    },
    completed: {
      en: 'Completed',
      ru: 'Завершено',
    },
    nodeIsOffline: {
      en: 'Node is offline',
      ru: 'Нет связи с сервером',
    },
    networkName: {
      en: 'Network',
      ru: 'Сеть',
    },
    unknown: {
      en: 'Unknown',
      ru: 'Неизвестно',
    },
    blockNumber: {
      en: 'Block number',
      ru: 'Номер блока',
    },
  },

  nodeSettings: {
    nodeSettings: {
      en: 'Node settings',
      ru: 'Настройки сети',
    },
    recentNodes: {
      en: 'Recent nodes',
      ru: 'Недавние сервера',
    },
    selectRecentNode: {
      en: 'Select from recent nodes',
      ru: 'Выберите недавно используемый сервер',
    },
    couldNotConnectToNode: {
      en: 'Could not connect to node',
      ru: 'Нет связи с сервером',
    },
    nodeURL: {
      en: 'Node URL',
      ru: 'URL сервера',
    },
    chainId: {
      en: 'Chain ID',
      ru: 'Chain ID',
    },
  },

  importAccounts: {
    importAccounts: {
      en: 'Import accounts',
      ru: 'Загрузить счета',
    },
    accounts: {
      en: 'Accounts',
      ru: 'Счета',
    },
    notJSON: {
      en: 'Not a JSON file',
      ru: 'Не JSON файл',
    },
    noAddress: {
      en: 'Could not find valid account address',
      ru: 'Не найден адрес счета',
    },
    noPrivate: {
      en: 'Could not find private key',
      ru: 'Не найден закрытый ключ',
    },
    unsupportedVersion: {
      en: 'Unsupported keystore file version',
      ru: 'Неподдерживаемая версия формата файла',
    },
    alreadyExists: {
      en: 'Account already exists',
      ru: 'Счет уже существует',
    },
    openFiles: {
      en: 'Open files',
      ru: 'Открыть файлы',
    },
    import: {
      en: 'Import',
      ru: 'Загрузить',
    },
    errors: {
      en: 'Errors',
      ru: 'Ошибки',
    },
    fileName: {
      en: 'File name',
      ru: 'Имя файла',
    },
    errorMessage: {
      en: 'Error message',
      ru: 'Ошибка',
    },
    noFileSelected: {
      en: 'No files selected',
      ru: 'Не выбрано ни одного файла',
    },
    address: {
      en: 'Address',
      ru: 'Адрес',
    },
    name: {
      en: 'Name',
      ru: 'Имя',
    },
    remove: {
      en: 'Remove',
      ru: 'Удалить',
    }
  },

  form: {
    cancel: {
      en: 'Cancel',
      ru: 'Отмена',
    },
    ok: {
      en: 'Ok',
      ru: 'Ok',
    },
    apply: {
      en: 'Apply',
      ru: 'Применить',
    },
    create: {
      en: 'Create',
      ru: 'Создать',
    },
    save: {
      en: 'Save',
      ru: 'Сохранить',
    },
    edit: {
      en: 'Edit',
      ru: 'Редактировать',
    },
    add: {
      en: 'Add',
      ru: 'Добавить',
    },
    remove: {
      en: 'Remove',
      ru: 'Удалить',
    },
    tryAgain: {
      en: 'Try again',
      ru: 'Попробовать еще раз',
    },
    cannotBeBlank: {
      en: 'Cannot be blank',
      ru: 'Не должно быть пустым',
    },
    password: {
      accountPassword: {
        en: 'Account password',
        ru: 'Пароль',
      },
      password: {
        en: 'Password',
        ru: 'Пароль',
      },
      repeatPassword: {
        en: 'Repeat password',
        ru: 'Повторите пароль',
      },
      passwordsDoNotMatch: {
        en: 'Passwords do not match',
        ru: 'Пароли не совпадают',
      },
      minLength: {
        en: 'at least % characters',
        ru: 'как минимум % символов',
      },
      invalid: {
        en: 'Invalid password',
        ru: 'Неправильный пароль',
      },
      newPassword: {
        en: 'New password',
        ru: 'Новый пароль',
      }
    },
    invalidAddress: {
      en: 'Invalid address',
      ru: 'Неверный формат адреса',
    },
    invalidNumber: {
      en: 'Invalid number',
      ru: 'Введите число',
    },
    search: {
      en: 'Search',
      ru: 'Поиск',
    },
    token: {
      en: 'Token',
      ru: 'Токен',
    }
  },

  common: {
    loading: {
      en: 'Loading',
      ru: 'Загрузка',
    },
    networkError: {
      en: 'Network problem',
      ru: 'Проблемы с сетью',
    },
    pleaseWait: {
      en: 'Please wait',
      ru: 'Пожалуйста подождите',
    },
    filterByToken: {
      en: 'Filter by token',
      ru: 'Отфильтровать по токену',
    },
    nocontent: {
      en: 'No data found',
      ru: 'Ничего не найдено',
    },
    nodeIsOffline: {
      en: 'Node is offline',
      ru: 'Нет связи с сервером',
    },
    price: {
      unknown: {
        en: 'Unknown',
        ru: 'Неизвестно',
      }
    },
    amount: {
      en: 'Amount',
      ru: 'Сумма',
    },
    timeInterval: {
      title: {
        en: 'Data scale',
        ru: 'Временной интервал',
      },
      week: {
        en: 'Week',
        ru: 'Неделя',
      },
      month: {
        en: 'Month',
        ru: 'Месяц',
      },
      year: {
        en: 'Year',
        ru: 'Год',
      },
    },
    accountSelect: {
      accounts: {
        en: 'Accounts',
        ru: 'Счета',
      },
      addresses: {
        en: 'Addresses',
        ru: 'Адреса',
      },
    },
  },

}


})
