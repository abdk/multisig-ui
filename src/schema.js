define(function(require){

var {
  Bool, Num, NumWithLimits, Str, Re, BigNum, Dt, Enum,
  Either, Optional, Arr, ObjMap, Obj, FreeFormObj
} = require('utils/schema')

var Address = Re(/^0x[\da-f]{40}$/)

var Hash = Re(/^0x[\da-f]{64}$/)

var NotEmptyString = Re(/.+/)

var Transaction = Obj({
  transaction: FreeFormObj({
    // don't specify it completely, just several fields
    hash: Hash,
    from: Address,
    value: Optional(BigNum),
  }),
  receipt: Optional(FreeFormObj({
    blockHash: Hash,
  })),
  // TODO
  metadata: FreeFormObj({
    date: Dt,
    operation: Optional(Str),
    isSingleTransact: Optional(Bool),
    value: Optional(BigNum),
  })
})

var mainDBSchema = {
  SCHEMA_VERSION: Num,
  knownChains: Arr(Num),
  recentNodes: Arr(Obj({
    id: Num,
    chainId: Num,
    firstBlockHash: Hash,
    name: NotEmptyString,
    type: Enum('privatenet', 'mainnet', 'testnet'),
    url: NotEmptyString,
  })),
  nodeId: Optional(Num),
  catalogNodeId: Optional(Num),
  tokens: Arr(Obj({
    address: Address,
    name: NotEmptyString,
    decimals: Optional(Num),
  })),
  walletBook: Arr(Obj({
    address: Address,
    name: NotEmptyString,
  })),
}

function createDB(storage){
  storage.saveToMainDB('knownChains', [])
  storage.saveToMainDB('recentNodes', [])
  storage.saveToMainDB('tokens', [])
  storage.saveToMainDB('walletBook', [])
}

var chainDBSchema = {
  transactions: Arr(Transaction),
  wallets: Arr(Obj({
    transactionHash: Hash,
    date: Dt,
    owners: Optional(Arr(Address)),
    required: Optional(Num),
    daylimit: Optional(BigNum),
    address: Optional(Address),
    walletType: Optional(Enum('tokenFriendly', 'standard')),
  })),
  recentAddresses: Arr(Address),
}

function createChainDB(storage, chainId){
  storage.save({
    transactions: [],
    wallets: [],
    recentAddresses: [],
  })
}

function createChainIndexedDB(chainId){
}

var migrations = [
  function(storage){
    storage.saveToMainDB('walletBook', [])
  }
];

return {createDB, createChainDB, createChainIndexedDB, mainDBSchema, chainDBSchema, migrations}

});
