define(function(require){

// Global emitter for notifying react component about state changes
var emitter = new EventEmitter();

window.state = {ui: {}}

function stateChanged(ev){
  emitter.trigger(ev);
}

// subscribes react component to state changes
function connect(ev, component) {
  return React.createClass({

    onEvent(){
      this.forceUpdate();
    },

    componentWillMount(){
      emitter.on(ev, this.onEvent);
    },

    componentWillUnmount(){
      emitter.off(ev, this.onEvent);
    },

    render(){
      return React.createElement(component, this.props, this.props.children);
    }

  });
}

/*
Creates valueLink to the prop of the object. This valueLink notifies
about state changes when change is requested

see
https://facebook.github.io/react/docs/two-way-binding-helpers.html#valuelink-without-linkedstatemixin
*/

function link(obj, propName){
  return {
    value: obj[propName],

    requestChange: function(value){
      obj[propName] = value;
    },

    andThen: function(fn){
      var oldRequestChange = this.requestChange;
      return Object.assign(
        this, 
        {
          requestChange: function(val){
            oldRequestChange(val);
            fn(val);
          }
        }
      )
    }
  };
}

function bindAll(obj) {
  for (var key in obj) {
    var val = obj[key];
    if (typeof val === 'function') {
      obj[key] = val.bind(obj)
    }
  }
  return obj;
}

function Stateful(getInitialState, Comp){

  function componentStateChanged(){
    stateChanged('component')
  }

  function link(prop){
    return {
      value: state.ui.component[prop],
      requestChange(val){
        state.ui.component[prop] = val
        componentStateChanged()
      }
    }
  }

  return React.createFactory(React.createClass({

    onEvent(){
      this.forceUpdate()
    },

    componentWillMount(){
      var comp = state.ui.component = bindAll(getInitialState({
        stateChanged: componentStateChanged,
      }))
      emitter.on('component', this.onEvent);
      if(comp.dataChanged != null){
        for(var key in comp.dataChanged){
          var cb = comp.dataChanged[key].bind(comp)
          comp.dataChanged[key] = cb
          onDataChanged(key, cb)
        }
      }
    },

    componentWillUnmount(){
      var comp = state.ui.component
      delete state.ui.component
      emitter.off('component', this.onEvent);
      if(comp.dataChanged != null){
        for(var key in comp.dataChanged){
          offDataChanged(key, comp.dataChanged[key])
        }
      }
    },

    render(){
      return Comp({state: state.ui.component, link, stateChanged: componentStateChanged})
    }

  }))

}

function getComponent(fn){
  return state.ui.component
}


function dataChanged(ev){
  emitter.trigger('data:' + ev);
}

function onDataChanged(ev, cb){
  emitter.on('data:'+ev, cb)
}

function offDataChanged(ev, cb){
  emitter.off('data:'+ev, cb)
}

return { 
  stateChanged,
  dataChanged,
  onDataChanged,
  offDataChanged,
  connect,
  Stateful,
  getComponent,
  link,
  on: emitter.on.bind(emitter),
  off: emitter.off.bind(emitter),
};


});
