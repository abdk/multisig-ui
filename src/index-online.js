define(function(require){

var config = require('config')
var ethNode = require('./ethNode')
var storage = require('browserStorage')
var initEthNode = require('./initEthNode')
var crashReport = require('crashReport')
var i18n = require('i18n')
var NavLayout = require('widgets/navLayout')
var {NodeAddress} = require('ui/nodeAddress')
var DeployWalletOnline = require('ui/deployWalletOnline')
var Wallets = require('ui/wallets')
var SendTransaction = require('ui/sendTransaction')
var VerifyTransaction = require('ui/verifyTransaction')
var Transactions = require('ui/transactions')
var GetNonce = require('ui/getNonce')

var {span, a} = React.DOM

crashReport.init()
i18n.initWithDefaultLang()
storage.init()

function render(){
  document.body.removeChild(document.body.children[0])
  var container = document.createElement('div')
  document.body.appendChild(container)
  ReactDOM.render(React.DOM.div(null, 
    NodeAddress(),
    ethNode.isNodeConfigured() &&
      NavLayout({
        title: span(null, 
          'Ethereum Multisig Online ' + config.APP_VERSION,
          a({
            href: 'offline.html', 
            target: '_blank', 
            style: {marginLeft: '20px', textDecoration: 'underline'}
          },
            '(open Ethereum Multisig Offline)'
          )
        ),
        isShowNodeInfo: true,
        items: [
          {
            value: 'deployWallet', 
            label: 'Deploy Wallet Online', 
            content: DeployWalletOnline,
          },
          {
            value: 'wallets',
            label: 'Wallets',
            content: Wallets,
          },
          {
            value: 'getNonce',
            label: 'Get Nonce',
            content: GetNonce,
          },
          {
            value: 'sendTransaction',
            label: 'Send Transaction',
            content: SendTransaction,
          },
          {
            value: 'verifyTransaction',
            label: 'Operation Details',
            content: VerifyTransaction,
          },
          {
            value: 'transactions',
            label: 'Transactions',
            content: Transactions,
          },
        ],
        defaultItem: 'deployWallet',
      })
    ),
    container
  )
}

if(ethNode.isNodeConfigured()){
  initEthNode()
  state.nodeInfo.fetchNodeInfo().then(render)
} else {
  state.ui.isShowNodeAddress = true
  render()
}

})
