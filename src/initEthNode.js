define(function(require){

var core = require('core')
var storage = require('browserStorage')
var ethNode = require('./ethNode')

return function(){
  storage.initChain(ethNode.getChainId())
  ethNode.initWeb3()
  state.nodeInfo = ethNode.NodeInfo()
  if(state.nodeInfoUpdater == null){
    state.nodeInfoUpdater = ethNode.NodeInfoUpdater()
    state.nodeInfoUpdater.runUpdatingNodeInfo()
  }
}

})
