define(function(require){

var Utils = {

  promisedProperties(object) {
    var props = [];
    var objectKeys = Object.keys(object);
    objectKeys.forEach((key) => props.push(object[key]));
    return Promise.all(props).then((resolvedValues) => {
      var result = {};
      for(var i = 0; i < objectKeys.length; i++){
        result[objectKeys[i]] = resolvedValues[i];
      }
      return result;
    });
  },

  fromHumanizedValue(amount,token){
    if(token == 'ether'){
      return web3.toWei(amount, 'ether')
    } else {
      var decimals = token.decimals
      decimals = decimals || 0
      return amount.shift(decimals)
    }
  },

  effectiveTokenAmount(amount, token){
    if(token == 'ether'){
      return web3.fromWei(amount)
    } else {
      var decimals = token.decimals
      decimals = decimals || 0
      return amount.shift(-decimals)
    }
  },

  toHumanizedValue(amount,token){
    var effectiveAmount = Utils.effectiveTokenAmount(amount, token)
    return effectiveAmount.toFormat(2)
  },

  normalizeAddress(address){
    return (address.indexOf('0x') === 0 ? address : '0x' + address)
      .toLowerCase()
  },
  
  formatWei(value){
    return web3.fromWei(value, 'ether')
  },

  readFile(file){
    var reader = new FileReader();
    var p = new Promise(function(resolve){
      reader.onload = function(e) {
        resolve(e.target.result)
      }
    })
    reader.readAsText(file);
    return p
  },

  readFiles(fileList){
    var files = []
    for(var i = 0; i < fileList.length; i++){
      files.push(fileList[i]);
    }
    return Promise.all(files.map(Utils.readFile))
  },

  openFile: (() => {
    // reuse single input instance to workaround this bug:
    // https://stackoverflow.com/questions/39399947/event-onchange-wont-trigger-after-files-are-selected-from-code-generated-input
    // https://stackoverflow.com/questions/38845015/inputtype-file-change-event-are-lost-occasionally-on-chrome
    var input = document.createElement('input')
    input.setAttribute('type','file')
    var inputPromiseResolve
    input.addEventListener('change', function({target}){
      inputPromiseResolve(target.files);
    });
    return options => {
      options = options ? options : {};
      var accept = options.accept;
      if(options.multiple){
        input.setAttribute('multiple','true');
      } else {
        input.removeAttribute('multiple');
      }
      if(accept){
        input.setAttribute('accept', accept);
      } else {
        input.removeAttribute('accept');
      }
      var p = new Promise(resolve => inputPromiseResolve = resolve)
      input.click()
      return p
    }
  })(),

  timeout(promise, timeoutMs){
    var timeoutId
    return Promise.race(
      promise,
      new Promise((resolve, reject) => {
        timeoutId = setTimeout(() => reject({isTimeout: true}, timeoutMs))
      }).then((result) => {
        clearTimeout(timeoutId)
        return result
      })
    )
  },

}

return Utils

});
