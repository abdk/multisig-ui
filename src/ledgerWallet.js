define(function(require){

var HD_DERIVATION_PATH_BASE = "44'/60'/0'"

function getLedgerAddress(hdDerivationPath){
  return new Promise((resolve, reject) => {
    ledger.comm_u2f.create_async().then(function(comm) {
      resolve(new ledger.eth(comm))
    })
  }).then(eth => {
    return new Promise((resolve, reject) => {
      eth.getAddress_async(hdDerivationPath)
        .then(resolve)
        .fail(reject)
    })
  })
}

function getLedgerAddresses(){
  return Promise.all(_.range(5).map(i =>
    getLedgerAddress(HD_DERIVATION_PATH_BASE + '/' + i)
  ))
}

function applyEIP155(tx, chainId){
  var old = false
  tx.raw[6] = EthJS.Buffer.Buffer.from([chainId])
  tx.raw[7] = tx.raw[8] = 0
  var toHash = (chainId == 0) ? tx.raw.slice(0, 6) : tx.raw
  return EthJS.Util.rlp.encode(toHash)
}

function signTransactionWithLedger(txParams, addressIndex){
  var hdDerivationPath = HD_DERIVATION_PATH_BASE + '/' + addressIndex
  return new Promise((resolve, reject) => {
    ledger.comm_u2f.create_async().then(function(comm) {
      resolve(new ledger.eth(comm))
    })
  }).then(eth => {
    return new Promise((resolve, reject) => {
      eth.getAddress_async(hdDerivationPath)
        .then(resolve)
        .fail(reject)
    }).then(({address}) => {
      var tx = new EthJS.Tx(txParams)
      var serializedTx = applyEIP155(tx, txParams.chainId).toString('hex')
      return new Promise((resolve, reject) => {
        eth.signTransaction_async(hdDerivationPath, serializedTx)
          .then(resolve)
          .fail(reject)
      })
      .then(rsv => ({rsv, address}))
    })
  }).then(({rsv, address}) => {
    txParams.from = address
    Object.assign(txParams, rsv)
    txParams.r = Web3.prototype.toHex('0x' + txParams.r)
    txParams.s = Web3.prototype.toHex('0x' + txParams.s)
    txParams.v = Web3.prototype.toHex('0x' + txParams.v)
    var tx = new EthJS.Tx(txParams)
    var serializedTx = '0x' + tx.serialize().toString('hex')
    return serializedTx
  })
}

return {getLedgerAddresses, signTransactionWithLedger}

})
