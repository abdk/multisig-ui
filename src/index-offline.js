define(function(require){

var config = require('config')
var storage = require('browserStorage')
var i18n = require('i18n')
var NavLayout = require('widgets/navLayout')
var DeployWalletOffline = require('ui/deployWalletOffline')
var CreateTransaction = require('ui/createTransaction')
var CreateBurnTokensTransaction = require('ui/createBurnTokensTransaction')
var ConfirmTransaction = require('ui/confirmTransaction')
var makeEntitiesComponent = require('ui/entities')

var {span, a} = React.DOM

var Tokens = makeEntitiesComponent({
  entityName: 'token', 
  storageKey: 'tokens',
  withDecimals: true,
})

var WalletBook = makeEntitiesComponent({
  entityName: 'wallet', 
  storageKey: 'walletBook',
  withDecimals: false,
})

window.log = console.log
window.error = console.error

i18n.initWithDefaultLang()
storage.init()

function render(){
  document.body.removeChild(document.body.children[0])
  var container = document.createElement('div')
  document.body.appendChild(container)
  ReactDOM.render(
    NavLayout({
      title: span(null, 
        'Ethereum Multisig Offline ' + config.APP_VERSION + ' ',
        !window.location.protocol.startsWith('file') &&
          a({}, 'select "File" -> "Save Page As" or just press Ctrl-S to download for offline usage')
      ),
      isShowNodeInfo: false,
      items: [
        {
          value: 'tokens',
          label: 'Tokens',
          header: Tokens.Header,
          content: Tokens.List,
        },
        {
          value: 'wallets',
          label: 'Wallets',
          header: WalletBook.Header,
          content: WalletBook.List,
        },
        {
          value: 'deployWallet', 
          label: 'Deploy Wallet Offline', 
          content: DeployWalletOffline,
        },
        {
          value: 'createTransaction',
          label: 'Create Transaction',
          content: CreateTransaction,
        },
        {
          value: 'createBurnTokensTransaction',
          label: 'Create Burn Tokens Transaction',
          content: CreateBurnTokensTransaction,
        },
        {
          value: 'confirmTransaction',
          label: 'Confirm Transaction',
          content: ConfirmTransaction,
        },
      ],
      defaultItem: 'createTransaction',
    })
    ,
    container
  )
}

if(document.body && document.body.children && document.body.children[0]){
  render()
} else {
  window.addEventListener('load', render)
}

})
