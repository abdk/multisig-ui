define(function(require){

var config = require('config')
var {callWeb3} = require('utils/web3');
var {promisedProperties} = require('utils');
var ethNode = require('./ethNode');
var storage = require('browserStorage');
var ledgerWallet = require('ledgerWallet')

function txToHex(txParams){
  var web3 = new Web3()
  var tx = {
    from: txParams.from,
    to: txParams.to,
    nonce: web3.toHex(txParams.nonce),
    value: web3.toHex(txParams.value),
    gasPrice: web3.toHex(txParams.gasPrice),
    chainId: txParams.chainId,
  }
  if(txParams.gasLimit != null){
    tx.gasLimit = web3.toHex(txParams.gasLimit)
  }
  if(txParams.data != null){
    tx.data = web3.toHex(txParams.data)
  }
  return tx
}

function serializeTx(tx){
  return '0x' + tx.serialize().toString('hex')
}

function contractAddress(tx){
  return '0x' + 
    EthJS.Util.sha3(EthJS.Util.rlp.encode([tx.from, tx.nonce]))
    .toString('hex')
    .slice(24)
}

function decipherPrivateKey(address, password, keyFile){
  try{
    var w = Wallet.fromV3(JSON.stringify(keyFile),password,true)
  }catch(e){
    return {isValidPassword: false}
  }
  return {isValidPassword: true, privateKey: w.getPrivateKeyString()}
}

function signTransactionWithKeyFile(txParams, {password, keyFile}){
  txParams.from = keyFile.address
  txParams = txToHex(txParams)
  log('txParams', txParams)
  var {isValidPassword, privateKey} = 
    decipherPrivateKey(txParams.from, password, keyFile)
  if(!isValidPassword){
    throw {message: 'Invalid password', isInvalidPassword: true}
  }
  var keyBuffer = EthJS.Buffer.Buffer.from(privateKey, 'hex')
  var tx = new EthJS.Tx(txParams)
  tx.sign(keyBuffer)
  return tx
}

function prepareTransaction(tx){
  log('prepareTransaction', tx)
  var chainId = ethNode.getChainId()
  return promisedProperties({
    gasPrice: callWeb3(web3.eth.getGasPrice)(),
    estimatedGasLimit: callWeb3(web3.eth.estimateGas)(txToHex(tx)),
    nonce: callWeb3(web3.eth.getTransactionCount)(tx.from, 'pending')
  })
  .then(({gasPrice, estimatedGasLimit, nonce}) => {
    var gasLimit = config.getGasLimit(estimatedGasLimit)
    return Object.assign({}, tx, {gasPrice, gasLimit, nonce, chainId})
  })
}

function signTransaction(tx, signData){
  return Promise.resolve().then(() => {
    if(signData.signWith == 'keyfile'){
      return serializeTx(signTransactionWithKeyFile(tx, signData))
    } else {
      return ledgerWallet.signTransactionWithLedger(
        txToHex(tx), 
        signData.addressNumber
      )
    }
  })
}

function sendTransaction(tx, signData){
  return Promise.resolve().then(() => {
  if(signData.signWith == 'keyfile'){
    tx.from = signData.keyFile.address
  } else {
    return ledgerWallet.getLedgerAddresses().then(addresses => {
      tx.from = addresses[signData.addressNumber].address
    })
  }
  }).then(() => {
    return prepareTransaction(tx)
      .then(tx => signTransaction(tx, signData))
      .then(signedTx => callWeb3(web3.eth.sendRawTransaction)(signedTx))
      .then(hash => {
        log('tx accepted', hash)
        return Object.assign({}, tx, {hash})
      })
  })
}

return {
  signTransaction,
  sendTransaction, 
}

})
