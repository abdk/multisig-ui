define(function(){

var {a, span} = React.DOM

return React.createFactory(React.createClass({
  getInitialState(){
    return {copied: false}
  },

  onCopy(e){
    e.preventDefault()
    clipboard.copy(this.props.value)
    this.setState({copied: true})
    setTimeout(() => this.setState({copied: false}), 1500)
  },

  render(){
    return span({
      className: 'copy-container', 
      style: this.props.isInline ? {display: 'inline'} : null
    },
      this.props.children,
      a({
        className: 'copy-button',
        onClick: this.onCopy,
      },
        this.state.copied
        ?  'Copied!'
        :  'Copy'
      )
    )
  },
}))

})
