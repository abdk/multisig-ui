define(function(require){

var FormGroup = require('widgets/formGroup')

return React.createFactory(React.createClass({

  getInitialState(){
    return {wasChanged: false}
  },

  onChange(val){
    this.setState({wasChanged: true})
    this.props.valueLink.requestChange(val)
  },

  render(){
    var showError = this.state.wasChanged && this.props.valueLink.value == null
    var props = Object.assign({}, this.props, {
      error: showError ? this.props.error : null,
      input: this.props.input({
        valueLink: {
          value: this.props.valueLink.value,
          requestChange: this.onChange,
        },
      }),
    })
    return FormGroup(props)
  },

}))


})
