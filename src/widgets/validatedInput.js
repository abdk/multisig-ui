define(function(){

return function(InputComp, {fromString, toString}){

  return React.createFactory(React.createClass({
    getInitialState(){
      return {value: toString(this.props.valueLink.value)};
    },

    componentWillReceiveProps(newProps){
      var newValue = newProps.valueLink.value
      if(newValue != fromString(this.state.value)){
        this.setState({value: toString(newValue)});
      }
    },

    render(){
      var props = Object.assign({}, this.props, {valueLink: {
        value: this.state.value,
        requestChange: this.requestChange
      }});
      return InputComp(props);
    },

    requestChange(str){
      var value = fromString(str)
      this.setState({value: str}, function(){
        this.props.valueLink.requestChange(value);
      });
    }

  }));

}

});
