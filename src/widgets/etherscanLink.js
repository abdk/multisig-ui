define(function(require){

var {a, span} = React.DOM

var ethNode = require('ethNode')

return React.createFactory(React.createClass({
  componentWillMount(){
    this.chainId = ethNode.getChainId()
  },

  render(){
    var {address, children, className} = this.props
    var type
    if(address.length == 42){
      type = 'address'
    } else if(address.length == 66){
      type = 'tx'
    } else {
      throw new Error('unknown type: ' + address)
    }
    if([1,3].indexOf(this.chainId) != -1){
      var urlBase = this.chainId == 1 /* Mainnet */
        ? 'https://etherscan.io/'
        : 'https://ropsten.etherscan.io/'
      return a({
        className, 
        href: urlBase + '/' + type + '/' + address, 
        target: '_blank'
      },
        children
      )
    }else{
      return span(null,
        children
      )
    }
  },

}))

})
