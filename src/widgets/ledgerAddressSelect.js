define(function(require){

var {normalizeAddress} = require('utils')
var ledgerWallet = require('ledgerWallet')
var FormGroup = require('widgets/formGroup')
var FormMessage = require('widgets/FormMessage')
var AccountSelect = require('widgets/accountSelect')
var {div, span, button, form} = React.DOM

return React.createFactory(React.createClass({

  getInitialState(){
    return {}
  },

  componentDidMount(){
    this.fetchAddresses()
  },

  setAddress(address){
    var addressNumber = this.state.addresses
      .map(addr => addr.address)
      .indexOf(address)
    this.setState({addressNumber}, this.onChange)
  },

  onChange(){
    var address = this.state.addresses[this.state.addressNumber].address
    var value = {address, addressNumber: this.state.addressNumber}
    this.props.onChange(value)
  },

  fetchAddresses(){
    this.setState({status: 'loading'})
    ledgerWallet.getLedgerAddresses()
      .then(addresses => {
        this.setState({
          status: 'ok', 
          addresses: addresses.map(addr => 
            ({address: normalizeAddress(addr.address)})
          ),
          addressNumber: 0,
        }, this.onChange)
      })
      .catch(() => {
        this.setState({status: 'error'})
      })
  },

  render(){
    return div(null,
      this.state.status == 'loading' && 
        FormMessage({
          status: 'loading', 
          message: 'Loading addresses from Ledger Wallet',
        }),
      this.state.status == 'error' && 
        FormMessage({
          status: 'error', 
          message: span({className: 'hwallet-error'}, 
            'Could not load addresses from Ledger Wallet',
            button({
              className: 'btn btn-default',
              type: 'button',
              onClick: this.fetchAddresses,
            }, 'Try again')
          )
        }),
      this.state.status == 'ok' &&
        FormGroup({
          labelMsg: 'Address',
          input: AccountSelect({
            accounts: this.state.addresses,
            value: this.state.addresses[this.state.addressNumber].address,
            onChange: this.setAddress,
            placeholder: 'Address',
          }),
          noError: true,
        })
    )
  },

}))

})
