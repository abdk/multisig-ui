define(function(require){

var {span, div} = React.DOM

function formatDecimals(decimals){
  var GROUP_SIZE = 3
  return [_.range(decimals % GROUP_SIZE).map(_ => '0').join('')]
    .concat(_.range(Math.floor(decimals / GROUP_SIZE)).map(_ => '000'))
    .join(',')
}

return function({token}){
  return (token != 'ether') && (
    token.decimals == null
      ? span({className: 'decimals'},
          div(null,  'unknown'),
          div(null,  'decimals')
        )
      : span({className: 'decimals'},
          div({className: 'decimals-value'}, 
            '× 1' + formatDecimals(token.decimals)),
          div({className: 'decimals-label'}, 'decimals')
        )
  )
}

})
