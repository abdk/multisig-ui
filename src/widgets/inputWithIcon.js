define(function(){

var {div, span, input, i} = React.DOM;

return React.createFactory(function(props){
  return div({className: 'input-group input-with-icon'},
    input({
      className: 'form-control', 
      valueLink: props.valueLink,
      type: props.type,
      name: props.name
    }),
    span({className: 'input-group-addon', onClick: props.onIconClick},
      i({className: 'fa fa-' + props.icon})
    )
  )
});

});
