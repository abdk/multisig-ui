define(function(require){

var ValidatedFormGroup = require('./validatedFormGroup')
var AddressInput = require('widgets/addressInput')

return React.createFactory(function(props){
  return ValidatedFormGroup({
    labelMsg: props.labelMsg,
    input: ({valueLink}) => AddressInput({
      disabled: props.disabled,
      name: props.name,
      className: 'form-control',
      autoFocus: props.autoFocus, 
      valueLink
    }),
    valueLink: props.valueLink,
    error: 'Not a valid address',
  })
})

})
