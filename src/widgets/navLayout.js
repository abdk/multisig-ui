define(function(require){

var Layout = require('./layout')

function currentRoute(){
  return window.location.hash.slice(1)
}

function navigate(hash){
  window.location.hash = hash
}

return React.createFactory(React.createClass({

  componentWillMount(){
    window.addEventListener('hashchange', this.onHashChange)
  },

  componentWillUnmount(){
    window.removeEventListener('hashchange', this.onHashChange)
  },

  onHashChange(){
    this.forceUpdate()
  },

  render(){
    return Layout({
      items: this.props.items,
      selectedItem: currentRoute() || this.props.defaultItem,
      title: this.props.title,
      isShowNodeInfo: this.props.isShowNodeInfo,
    })
  },

}))

})
