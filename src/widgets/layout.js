define(function(require){

var {ul, li, div, a} = React.DOM
var NodeInfo = require('ui/nodeInfo')

var TEMPLATE = `
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <a class="navbar-brand"></a>
          <div class='nodeInfo-wrapper'>
          </div>
        </div>
      </div>
    </nav>

    <div class="app-content">
        <div class="sidebar">
          <a 
            class="logo" 
            href="https://abdk.consulting" 
            target=_blank
            title='ABDK'
          >
          </a>
          <div class="sidebar-content"></div>
        </div>
        <div class="main">
          <h1 class="page-header"></h1>
          <div class="layout-content">
          </div>
        </div>
      </div>
    </div>
`

return React.createFactory(React.createClass({

  componentDidMount(){
    this.el = ReactDOM.findDOMNode(this)
    this.el.innerHTML = TEMPLATE
    this.sidebar = this.el.getElementsByClassName('sidebar-content')[0]
    this.brand = this.el.getElementsByClassName('navbar-brand')[0]
    this.header = this.el.getElementsByClassName('page-header')[0]
    this.content = this.el.getElementsByClassName('layout-content')[0]
    this.nodeInfoWrapper = this.el.getElementsByClassName(
      'nodeInfo-wrapper'
    )[0]
    this.applyDomChanges()
  },

  componentDidUpdate(){
    this.applyDomChanges()
  },

  getSelectedItem(){
    return this.props.items.find(item => item.value == this.props.selectedItem)
  },

  applyDomChanges(){
    var label = this.getSelectedItem().label
    var header = this.getSelectedItem().header || this.getSelectedItem().label
    if(typeof(header) == 'string'){
      this.header.innerText = header
    } else {
      ReactDOM.render(header, this.header)
    }
    ReactDOM.render(this.getSelectedItem().content(), this.content)
    this.renderSidebar()
    if(this.props.isShowNodeInfo){
      ReactDOM.render(NodeInfo(), this.nodeInfoWrapper)
    }
    ReactDOM.render(this.props.title, this.brand)
  },

  renderSidebar(){
    ReactDOM.render(
      ul({className: "nav nav-sidebar"},
        this.props.items.map(item => 
          li({
            key: item.value, 
            className: item == this.getSelectedItem() ? 'active' : null,
          },
            a({href: '#' + item.value},
              item.label
            )
          )
        )
      )
      ,
      this.sidebar
    )
  },

  render(){
    return div({className: 'app'})
  },
}))

})
