define(function(require){

var FormMessage = require('./formMessage')
var Account = require('./account')
var {parseFile} = require('importAccounts')
var {normalizeAddress} = require('utils')
var {openFile, readFile} = require('utils/file')
var copy = require('widgets/copy')
var {div, input, button, span} = React.DOM

return React.createFactory(React.createClass({

  getInitialState(){
    return {isKeyFileOpened: false}
  },

  openKeyFile(){
    openFile().then(files => readFile(files[0])).then(
      content => {
        var {isValid, message, keyData} = parseFile({
          content, file: {}
        })
        this.setState({isKeyFileOpened : true, isValid, keyData, message})
        this.props.onChange(isValid
          ? Object.assign({}, keyData, {
            address: normalizeAddress(keyData.address)
          })
          : null
        )
      }
    )
  },

  render(props){
    return div(null,
      div({className: 'open-keyfile-group'},
        button({
          className: 'btn btn-primary',
          onClick: this.openKeyFile,
          type: 'button',
        },
          'Open key file'
        ),
        this.state.isKeyFileOpened && (
          this.state.isValid
          ? copy({value: Web3.prototype.toChecksumAddress(this.state.keyData.address)},
              Account({address: Web3.prototype.toChecksumAddress(this.state.keyData.address)})
            )
          : FormMessage({
              status: 'error', 
              message: 'Invalid key file: ' + this.state.message,
            })
        )
      )
    )
  }
}))


})
