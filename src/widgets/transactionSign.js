define(function(require){

var Select = require('widgets/select')
var Button = require('widgets/button')
var PasswordFormGroup = require('widgets/passwordFormGroup')
var OpenKeyFile = require('widgets/openKeyFile')
var FormGroup = require('widgets/formGroup')
var LedgerAddressSelect = require('./ledgerAddressSelect')
var copy = require('widgets/copy')
var {div, span, button, form} = React.DOM

var isLedgerWalletAvailable = window.location.protocol.startsWith('https')

class SignData {
  constructor(data){
    Object.assign(this, data)
  }

  getAddress(){
    return this.signWith == 'keyfile'
      ? this.keyFile.address
      : this.ledgerAddress
  }
}

function linkInto(link, prop){
  return {
    value: link.value[prop],
    requestChange(val){
      var nextValue = _.clone(link.value)
      nextValue[prop] = val
      link.requestChange(nextValue)
    }
  }
}

function SignWithKeyFileOnly({valueLink}){
  return div(null,
    FormGroup({
      labelMsg: 'Sender',
      input: OpenKeyFile({
        onChange(keyFile){
          valueLink.requestChange({keyFile})
        }
      }),
    }),
    valueLink.value.keyFile != null &&
      PasswordFormGroup({
        address: valueLink.value.keyFile.address,
        valueLink: linkInto(valueLink, 'password'),
      })
  )
}

function SignWithKeyFile({valueLink}){
  return div(null,
    OpenKeyFile({
      onChange(keyFile){
        valueLink.requestChange({keyFile})
      }
    }),
    valueLink.value.keyFile != null &&
      PasswordFormGroup({
        address: valueLink.value.keyFile.address,
        valueLink: linkInto(valueLink, 'password'),
      })
  )
}

return React.createFactory(React.createClass({

  mixins: [React.addons.LinkedStateMixin],

  getInitialState(){
    if(!isLedgerWalletAvailable){
      return {signWith: 'keyfile'}
    } else {
      return {}
    }
  },

  onChange(){
    var isValid
    var signData = new SignData({
      signWith: this.state.signWith,
    })
    if(signData.signWith == 'keyfile'){
      signData.keyFile = this.state.keyFile
      signData.password = this.state.password
      isValid = signData.keyFile != null
    }
    if(signData.signWith == 'ledger'){
      signData.addressNumber = this.state.addressNumber
      signData.ledgerAddress = this.state.ledgerAddress
      isValid = signData.addressNumber != null
    }
    this.props.valueLink.requestChange(isValid ? signData : null)
  },

  setSignWith(signWith){
    this.setState({signWith}, this.onChange)
  },

  setAddressNumber({address, addressNumber}){
    this.setState({addressNumber, ledgerAddress: address}, this.onChange)
  },

  render(){
    if(!isLedgerWalletAvailable){
      return SignWithKeyFileOnly({
        valueLink: {
          value: this.state,
          requestChange: ({keyFile, password}) => {
            this.setState({keyFile, password}, this.onChange)
          },
        }
      })
    }
    return div(null,
      FormGroup({
        labelMsg: 'Sender',
        input: Select({
          clearable: false,
          simpleValue: true,
          options: [
            {value: 'keyfile', label: 'Key file'},
            {value: 'ledger', label: 'Ledger Wallet'},
          ],
          valueLink: {
            value: this.state.signWith,
            requestChange: this.setSignWith,
          }
        }),
        noError: true,
      }),
      this.state.signWith == 'ledger' && 
        div({className: 'ledgerAddress-container'},
          LedgerAddressSelect({onChange: this.setAddressNumber}),
          this.state.ledgerAddress &&
            copy({value: Web3.prototype.toChecksumAddress(this.state.ledgerAddress)},
              null
            )
        ),
      this.state.signWith == 'keyfile' && SignWithKeyFile({
        valueLink: {
          value: this.state,
          requestChange: data => {
            this.setState(data, this.onChange)
          },
        }
      })
    )
  }
}))

})
