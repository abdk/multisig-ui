define(function(require){

var {div, span, input, h2, button, i, label, small, ul, li, a} = React.DOM;

return React.createFactory(React.createClass({
  render(){
    var options = this.props.options;
    return div({className: "header-dropdown"},
      div({className: "dropdown"},
        a({"data-toggle": 'dropdown'},
          i({className: 'fa fa-ellipsis-v'})
        ),
        ul({className: "dropdown-menu", id: this.dropdownId},
          options.map((option, i) => (
            li({onClick: option.onClick, key: i}, 
              a(null, option.name)
            )
          ))
        )
      )
    )
  }
}))

})
