define(function(){

var {button,i,span,div} = React.DOM;

var hiddenSpanStyle = {style: {visibility: 'hidden'}}

return React.createFactory(React.createClass({
  render(){
    var {
      onClick,
      type,
      title,
      disabled,
      btnType,
      isLoading
    } = this.props
    var className = 'btn btn-' + btnType;
    var spanStyle = isLoading ? hiddenSpanStyle : null
    return button({onClick: this.onClick,type,disabled,className},
      isLoading
        ? div({className: 'btn-spinner-container'},
            i({className: 'fa fa-refresh fa-spin'})
          )
        : null,
      title == null
        ? this.props.children
        : span(spanStyle, title)
    );
  },

  onClick(e){
    if(!this.props.isLoading){
      if(this.props.onClick != null){
        this.props.onClick(e);
      }
    }else{
      // prevent submitting form
      e.preventDefault()
    }
  }
}));

})
