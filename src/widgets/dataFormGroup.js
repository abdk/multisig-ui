define(function(require){

var FormGroup = require('widgets/formGroup')

var input = React.DOM.input

function parseHex(str){
  str = str.trim().toLowerCase()
  if(str.indexOf('0x') == 0){
    str = str.slice(2)
  }
  if(str == ''){
    return {isValid: true, value: null}
  } else if(/^[0-9a-f]+$/.test(str)){
    return {
      isValid: true, 
      value: '0x' + str
    }
  } else {
    return {isValid: false}
  }
}

return React.createFactory(React.createClass({

  getInitialState(){
    return {str: ''}
  },

  onChange(str){
    this.setState({str})
    var {isValid, value} = parseHex(str)
    this.props.valueLink.requestChange({isValid, value})
  },

  render(){
    return FormGroup({
      labelMsg: this.props.labelMsg,
      input: input({
        className: 'form-control',
        valueLink: {
          value: this.state.str,
          requestChange: this.onChange,
        },
      }),
      error: parseHex(this.state.str).isValid 
        ? null
        : 'Not a valid hex value',
    })
  },

}))


})
