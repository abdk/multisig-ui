define(function(require){

var AddressInput = require('widgets/addressInput')
var AccountSelect = require('widgets/accountSelect')
var FormGroup = require('widgets/formGroup')

var {div} = React.DOM

return React.createFactory(React.createClass({

  getInitialState(){
    var address
    var selectedAddress = this.props.addresses.find(address => 
      address == this.props.valueLink.value
    )
    if(selectedAddress == null){
      address = this.props.valueLink.value
    }
    return {
      hasError: false,
      address,
      selectedAddress,
    }
  },

  selectAddress(selectedAddress){
    this.setState({address: null, selectedAddress, hasError: false}, this.onChange)
  },

  setAddress(address){
    this.setState({address, selectedAddress: null, hasError: address == null}, this.onChange)
  },

  onChange(){
    this.props.valueLink.requestChange(
      this.state.address || this.state.selectedAddress
    )
  },

  render(){
    return FormGroup({
      labelMsg: this.props.labelMsg,
      error: this.state.hasError ? 'Invalid address' : null,
      input: div({className: 'addressInputOrSelect'},
        div(null,
          'Wallet address',
          AddressInput({
            className: 'form-control',
            name: this.props.name,
            autoFocus: this.props.autoFocus,
            valueLink: {
              value: this.state.address,
              requestChange: this.setAddress,
            },
          })
        ),
        div(null,
          'Select from deployed wallets',
          AccountSelect({
            clearable: true,
            placeholder: this.props.labelMsg,
            accounts: this.props.addresses.map(address => ({
              name: Web3.prototype.toChecksumAddress(address),
              address,
            })),
            valueLink: {
              value: this.state.selectedAddress,
              requestChange: this.selectAddress,
            },
          })
        )
      )
    })
  },

}))

})
