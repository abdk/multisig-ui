define(function(require){

var {div, span, input, h2, button, i, label, small, ul, li, a} = React.DOM;

return React.createFactory(function(props){
  var {tabs, activeTabLink} = props
  var activeTab = tabs.find(({id}) => id == activeTabLink.value)
  return div({className: 'tabs'},
    ul({className: 'nav nav-pills'},
      tabs.map((tab, i) => {
        var isActive = tab == activeTab
        return li({
            key: i, 
            className: isActive && 'active', 
            onClick: props.activeTabLink.requestChange.bind(null, tab.id)
          },
          a(null, tab.header)
        )
      })
    ),
    div({className: 'tab-content'},
      props.tabs.map((tab,i) => {
        var isActive = tab == activeTab
        var className = 'tab-pane fade ' + (isActive && 'active in')
        return div({className, key: i},
          tab.content()
        )
      })
    )
  )
})

});
