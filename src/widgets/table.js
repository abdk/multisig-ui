define(function(require){

var {table, tr, td, th, thead, tbody} = React.DOM

return React.createFactory(function(props){
  return table({className: props.className, style: props.style},
    thead(null,
      props.headers && tr(null,
        props.headers.map(header => th(null, header))
      )
    ),
    tbody(null,
      props.rows.map(row => 
        tr(null, row.map(cell => 
          td(null, cell)
        ))
      )
    )
  )
})

})
