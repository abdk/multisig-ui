define(function(require){

var {normalizeAddress} = require('utils');

var cache = {}

return React.createFactory(function({address}){
  address = normalizeAddress(address).toLowerCase()
  var image = cache[address]
  if(image == null){
    var url = blockies.create({
      seed: address,
      size: 8,
      scale: 8
    }).toDataURL();
    image = 'url("' + url + '")'
    cache[address] = image
  }
  return React.DOM.div({
    className: 'account-identicon', 
    style: {backgroundImage: cache[address]}
  })
});


})
