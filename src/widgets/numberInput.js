define(function(require){

function fromString(str){
  str = str.trim()
  if(str == ''){
    return null
  }
  var value = Number(str)
  if(isNaN(value)){
    return null
  }else{
    if(value < 0){
      // TODO compare with props.min and step instead of 0
      return null
    }else{
      return value
    }
  }
}

return React.createFactory(React.createClass({
  render(){
    var p = Object.assign({}, this.props, {
      onChange: (e) => {
        var num = fromString(e.target.value)
        this.props.valueLink.requestChange(num)
      },
      defaultValue: this.props.valueLink.value,
      type: 'number',
    })
    delete p.valueLink
    return React.DOM.input(p);
  },

  setValue(val){
    ReactDOM.findDOMNode(this).value = val
  }
}))


});
