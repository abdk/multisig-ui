define(function(require){

var Account = require('widgets/account');
var Select = require('widgets/select');
var {messages} = require('i18n');
var {form, div, span, input, h1, button, label} = React.DOM;

function accountRenderer({value,label}){
  if(value == null){
    return label;
  }else{
    return Account({
      address: value,
      name: label,
    });
  }
}

var AccountSelect = React.createFactory(function(props){
  var {
    disabled, 
    accounts, 
    clearable = false,
    addNullOption = false,
    placeholder = messages.transfer.account,
    valueLink,
    value,
    onChange,
  } = props
  if(valueLink == null){
    valueLink = {value, requestChange: onChange}
  }
  var options = accounts.map(({name,address}) => ({
    value: address,
    label: name
  }))
  if(addNullOption){
    options.unshift({value: null, label: placeholder})
  }
  return Select({
    simpleValue: true,
    placeholder,
    disabled,
    clearable,
    searchable: false,
    options,
    optionRenderer: accountRenderer,
    valueRenderer: accountRenderer,
    value: valueLink.value,
    onChange(value){
      valueLink.requestChange(value)
    }
  })
})

return AccountSelect

})
