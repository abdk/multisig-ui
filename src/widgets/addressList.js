define(function(require){

var AddressFormGroup = require('widgets/addressFormGroup')

var {a, div, span} = React.DOM

return React.createFactory(React.createClass({

  getInitialState(){
    return {
      currentId: 0,
      addresses: [
        {id: 0}
      ],
    }
  },

  componentDidUpdate(prevProps){
    if(prevProps.reservedAddress != this.props.reservedAddress){
      this.onChange()
    }
  },

  getAddresses(){
    // TODO check duplicating entries
    var result = this.state.addresses
    if(this.props.reservedAddress != null){
      result = result.filter(addr => addr.value != this.props.reservedAddress)
    }
    result = result.map(addr => addr.value)
    if(result.some(addr => addr == null)){
      result = null
    }
    return result
  },

  isNotEnoughAddresses(){
    var addresses = this.getAddresses() 
    if(addresses == null){
      return true
    } else {
      var count = addresses.length
      if(this.props.reservedAddress != null){
        count++
      }
      return count < 2
    }
  },

  onChange(){
    this.props.valueLink.requestChange(
      this.isNotEnoughAddresses()
        ? null
        : this.getAddresses()
    )
  },

  addAddress(e){
    e.preventDefault()
    var currentId = this.state.currentId
    this.state.addresses.push({id: ++currentId})
    this.setState({currentId}, this.onChange)
  },

  onChangeAddr(i, val){
    this.state.addresses[i].value = val
    this.onChange()
  },

  remove(i){
    this.state.addresses.splice(i, 1)
    this.forceUpdate()
    this.onChange()
  },

  displayedAddresses(){
    if(this.props.reservedAddress == null){
      return this.state.addresses
    } else {
      return [{
        id: 'reserved',
        value: this.props.reservedAddress,
        isReserved: true,
      }].concat(
        this.state.addresses.filter(addr => 
          addr.value != this.props.reservedAddress
        )
      )
    }
  },

  render(){
    return div({className: 'addressList'},
      this.displayedAddresses().map((addr, i) => {
        var idx = this.props.reservedAddress == null ? i : i - 1;
        return div({key: addr.id, className: 'addressRow'},
          AddressFormGroup({
            name: this.props.name,
            disabled: addr.isReserved,
            labelMsg: this.props.labelMsg,
            valueLink: {
              value: addr.value,
              requestChange: this.onChangeAddr.bind(null, idx),
            },
          }),
          !addr.isReserved &&
            a({className: 'remove', onClick: this.remove.bind(null, idx)}, 
            'Remove'
          )
        )
      }),
      a({href: '', className: 'add', onClick: this.addAddress},
        '+ Add owner'
      ),
      this.isNotEnoughAddresses() &&
        span({className: 'required'},
          'required at least two owners'
        )
    )
  },
}))


})
