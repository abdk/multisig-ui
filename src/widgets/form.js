define(function(require){

var iframe

// dummy html doc
var IFRAME_URL = "data:text/html;charset=utf-8,%3Chtml%3E%3Cbody%3Efoo%3C/body%3E%3C/html%3E"

return React.createFactory(React.createClass({

  componentWillMount(){
    if(iframe == null){
      iframe = document.createElement('iframe')
      iframe.setAttribute('name', 'iframe_for_autocomplete')
      iframe.setAttribute('style', 'width: 0px; height: 0px; position: "absolute", left: "-10000px"')
      iframe.setAttribute('src', IFRAME_URL)
      document.body.appendChild(iframe)
    }
  },

  onSubmit(e){
    if(this.props.onSubmit){
      this.props.onSubmit()
    }
  },

  render(){
    return React.DOM.form({
      target: 'iframe_for_autocomplete',
      onSubmit: this.onSubmit,
      className: this.props.className,
    }, 
      this.props.children
    )
  },

}))

})
