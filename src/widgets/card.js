define(function(require){

var Account = require('./account');
var HeaderDropdown = require('./headerDropdown');

var {div, span, input, h2, button, i, label, small, ul, li, a} = React.DOM;

return React.createFactory(function(props){
  var {title, subTitle, options} = props
  var className = "col-lg-4 col-md-6 col-sm-12 col-xs-12 "
  return div({className},
    div({className: "card"},
      div({className: "header"},
        h2(null, title, small(null, subTitle)),
        options && HeaderDropdown({options})
      )
    )
  )
})

});
